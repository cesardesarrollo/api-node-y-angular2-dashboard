import { Component,ElementRef,Renderer }  from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { LevelService }                 from './services/levels.service';
import { Level }                        from './level.model';
import { Router }                       from '@angular/router';

const styles    = require('./levels.css');
const template  = require('./levels.html');

@Component({
    selector    : 'levels',
    template    : template,
    styles      : [ styles ],
    providers   : [ LevelService ]
})

export class Levels {

    index: number;
    level: Level;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    idLevel: number;
    levels = [];

    constructor(private router: Router,
                private http: Http,
                private levelService: LevelService,
                private el: ElementRef,
                private render: Renderer){

        this.title = 'Niveles';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    ngOnInit(){ 
        if($('#menu-trigger').attr('class') != ''){
            $('#menu-trigger').toggleClass('open');
            $('#sidebar').toggleClass('toggled');
        }    
        this.renderData();
    }


    renderData(){
      //this.display = new DisplayComponent;
      //console.log(this.display);
        this.levelService.getAll().subscribe((result) => {
              if ( result.status ) {
                  this.levels = result.body;
                  let array = [];
                 /* for(let i = 0;i < this.levels.length;i++){
                        array.push(
                          [
                            this.levels[i].name,
                            this.levels[i].status  == true ? 'Activo': 'Inactivo',
                            "<a [routerLink]='['edit/',"+this.levels[i].id+"]'><i class='material-icons icon-size'>mode_edit</i></a>"
                          ]); 
                  }
                  $('#data-table').DataTable({
                    data: array,
                    columns: [
                          { title: "Nombre nivel" },
                          { title: "Estatus" },
                          { title: "Acciones" }   
                    ]});*/
              } 
            })
    }
    //$('#data-table').DataTable();

    showModal(event,idLevel,i){
      this.index = i;
      this.idLevel = idLevel;
      console.log("show modal");
      event.preventDefault();
      $("#modal-deactive").modal({
        show  : true,
        keyboard : false
      });
      $(".modal-backdrop").css( "z-index", "-1" );
    }

    deactiveRecord(){
          this.levelService.getById(this.idLevel)
                 .subscribe(
                    level =>  this.updateStatus(level),
                    error => console.log('Error: ' + error)
                 );
    }

    updateStatus(level){
         level.body.status = false;
         console.log(level.body.status);
         console.log(level.body);
         this.levelService.update(this.idLevel,level.body)
             .subscribe(
                result =>  {  
                if ( result ) {
                    console.log(this.levels);
                     this.levels.splice(this.index, 1);
                } else {}
       })
    }
}
