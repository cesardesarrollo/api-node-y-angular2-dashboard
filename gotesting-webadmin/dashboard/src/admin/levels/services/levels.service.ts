import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { enviroment } 	  from '../../../common/enviroments.ts';
import { Observable }     from 'rxjs/Rx';

const baseUrl = enviroment.base_url_api;

@Injectable()
export class LevelService {

	constructor( private http: Http){}

	create( name, status ) {
		console.log(name,status);
		let body = JSON.stringify({ name, status });
		let msg;
		return this.http
			.post(
				baseUrl + '/levels',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}

	getAll(){
		var filter = JSON.stringify({'where':{'status':'true'}});
		let msg;
		return this.http
			.get(
				baseUrl + '/levels?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getById(id){
		let msg;
		return this.http
			.get(
				baseUrl + '/levels/'+id,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	update(id,object){
		let body = JSON.stringify(object);
		let msg;
		return this.http
			.put(
				baseUrl + '/levels/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}
	
	private handleError (error: any) {
		//console.log(error);
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}

}