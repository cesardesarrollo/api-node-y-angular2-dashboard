import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { PlatformService }              from './services/platforms.service';
import { Platform }                     from './platforms.model';
import { Router }                       from '@angular/router';

const styles    = require('./platforms.css');
const template  = require('./platforms.html');

@Component({
    selector    : 'platforms',
    template    : template,
    styles      : [ styles ],
    providers   : [ PlatformService ]  
})

export class Platforms {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    index: number;
    platform: Platform;
    idPlatform: number;
    platforms = [];

    constructor(private router: Router, private http: Http, private platformService: PlatformService){
        this.title = 'Catálogo Plataformas';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }
    
    ngOnInit(){
        if($('#menu-trigger').attr('class') != ''){
            $('#menu-trigger').toggleClass('open');
            $('#sidebar').toggleClass('toggled');
        }   
        this.renderData();
    }

    renderData(){
        this.platformService.getAll().subscribe((result) => {
            if ( result.status ) {
                this.platforms = result.body;
            } else {
                //this.msg = 'Ocurrio un error al registrar';
                //$('#alert-danger').show();
            }
        })
    }

    showModal(event,idPlatform,i){
      this.index = i;
      this.idPlatform = idPlatform;
      event.preventDefault();
      $("#modal-deactive").modal({
        show  : true,
        keyboard : false
      });
      $(".modal-backdrop").css( "z-index", "-1" );
    }

    deactiveRecord(){
          this.platformService.getById(this.idPlatform)
                 .subscribe(
                    platform =>  this.updateStatus(platform),
                    error => console.log('Error: ' + error)
                 );   
    }

    updateStatus(platform){
        platform.body.status = false;
        this.platformService.update(this.idPlatform,platform.body)
          .subscribe(
              result =>  {  
              if ( result ) {
                  console.log(this.platforms);
                  this.platforms.splice(this.index, 1);
              } else {
                 //
              }
          })
        }
    }
