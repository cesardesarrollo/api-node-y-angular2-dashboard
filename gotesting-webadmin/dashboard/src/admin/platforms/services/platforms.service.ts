import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { enviroment } 	  from '../../../common/enviroments.ts';

const baseUrl = enviroment.base_url_api;

@Injectable()
export class PlatformService {

	constructor( private http: Http){}

	create( platform ) {
		let body = JSON.stringify( platform );
		console.log(body);
		let msg;
		return this.http
			.post(
				baseUrl + '/platforms',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			});
	}

	update(id, object){
		let body = JSON.stringify( object );
		let msg;
		return this.http
			.put(
				baseUrl + '/platforms/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			});
	}

	getById(id){
		let msg;
		return this.http
			.get(
				baseUrl + '/platforms/'+id,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}


	getAll(){
		var filter = JSON.stringify({'where':{'status':'true'}});
		let msg;
		return this.http
			.get(
				baseUrl +'/platforms?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}


}