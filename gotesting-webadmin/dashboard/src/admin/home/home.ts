import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./home.css');
const template  = require('./home.html');

@Component({
    selector    : 'admin-home',
    template    : template,
    styles      : [ styles ]
})

export class AdminHome {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;

    constructor( private router: Router, private http: Http ) { 
        this.title = 'Bienvenido';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    logout(){
        localStorage.removeItem('id_token');
        this.router.navigate(['/login']);
    }

    ngOnInit(){
        $('#menu-trigger').toggleClass('open');
        $('#sidebar').toggleClass('toggled');
    }

}
