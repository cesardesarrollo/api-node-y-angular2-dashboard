import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-course',
    template    : template,
    styles      : [ styles ]
})

export class CoursesCreate {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;

    constructor(private router: Router, private http: Http){
        this.title = 'Crear Curso';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }
    uploadFile(){
        $("#upload-file").click();
    }
}
