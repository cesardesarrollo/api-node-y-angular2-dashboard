import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./courses.css');
const template  = require('./courses.html');

@Component({
    selector    : 'courses',
    template    : template,
    styles      : [ styles ]
})

export class Courses {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;

    constructor(private router: Router, private http: Http){
        this.title = 'Catálogo Cursos';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    showDialog(){
        console.log("Mostrar dialogo");
        $('#modal-delete').modal('show');
    }
    ngOnInit(){
        $('#menu-trigger').toggleClass('open');
        $('#sidebar').toggleClass('toggled');     
    }
}
