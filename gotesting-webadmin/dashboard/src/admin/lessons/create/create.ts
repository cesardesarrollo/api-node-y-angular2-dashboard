import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { LessonService }                from '../services/lessons.service';
import { Lesson }                       from '../lesson.model';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-lesson',
    template    : template,
    styles      : [ styles ],
    providers   : [ LessonService ] 
})

export class LessonsCreate implements OnInit{
    lesson: Lesson;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    lessonForm: FormGroup;

    constructor(private fb: FormBuilder,private router: Router, private http: Http,private lessonService:LessonService){
        this.lesson = new Lesson(0,false,'','','','');
        this.title = 'Crear Lección';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    ngOnInit(){
         this.lessonForm = this.fb.group({
            name: '',
            description: '',
            status: '' 
        });
    }

    lessonCreate(){
        this.lessonService.create( this.lessonForm.value.name , this.lessonForm.value.description , this.lessonForm.value.status ).subscribe(
          (result) => {         
              this.onReturn();
        },
        (error)=>{ 
            if(error.status === 500){
                this.msg = 'El nombre de la Lección ya ha sido registrado, intenta con otro';
                $('#alert-danger').show();
                setTimeout(function() {
                        $('#alert-danger').hide();       
                }, 2000);
            } else {
                console.log("Status: "+error.status+" Message: "+error.statusText);
            }
        })
    }

    onReturn(){
        this.router.navigate(['/administrator/lessons'], {queryParams: {}});
    }

}
