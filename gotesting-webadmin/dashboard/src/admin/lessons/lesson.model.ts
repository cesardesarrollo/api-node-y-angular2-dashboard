export class Lesson {
  constructor(
  	public id: number,
    public status: boolean,
    public name: string,
    public description: string,
    public createdAt: string,
    public updatedAt: string
  ) {  }
}
