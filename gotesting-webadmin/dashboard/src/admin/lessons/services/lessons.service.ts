import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { enviroment } 	  from '../../../common/enviroments.ts';
import { Observable }     from 'rxjs/Rx';

const baseUrl = enviroment.base_url_api;

@Injectable()
export class LessonService {

	constructor( private http: Http){}


	create( name, description, status ) {
		let body = JSON.stringify({ name, description, status });
		let msg;
		return this.http
			.post(
				baseUrl + '/lessons',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}

	getAll(){
		let msg;
		return this.http
			.get(
				baseUrl + '/lessons',
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getById(id){
		let msg;
		return this.http
			.get(
				baseUrl + '/lessons/'+id,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	update(id,object){
		let body = JSON.stringify(object);
		let msg;
		return this.http
			.put(
				baseUrl + '/lessons/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}
	
	private handleError (error: any) {
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}

}