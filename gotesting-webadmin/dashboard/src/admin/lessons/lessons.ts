import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { LessonService }                 from './services/lessons.service';
import { Lesson }                        from './lesson.model';
import { Router }                       from '@angular/router';

const styles    = require('./lessons.css');
const template  = require('./lessons.html');

@Component({
    selector    : 'lessons',
    template    : template,
    styles      : [ styles ],
    providers   : [ LessonService ]
})

export class Lessons {
    idLesson: number;
    index: number;
    lesson: Lesson;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    lessons = [];

    constructor(private router: Router, private http: Http, private lessonService: LessonService){
        this.title = 'Lecciones';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }
    
    ngOnInit(){
        if($('#menu-trigger').attr('class') != ''){
             $('#menu-trigger').toggleClass('open');
             $('#sidebar').toggleClass('toggled');
        }   

       this.renderData();
    }

    renderData(){
        this.lessonService.getAll().subscribe((result) => {
              if ( result.status ) {
                  //console.log(result);
                  this.lessons = result.body;
                  //this.msg = 'Se registro un nivel';
                  //$('#alert-success').show();
              } else {
                  //this.msg = 'Ocurrio un error al registrar';
                  //$('#alert-danger').show();
              }
              ///this.cleanFields();
            })
    }

    showModal(event,idLesson,i){
      this.index = i;
      this.idLesson = idLesson;
      console.log("show modal");
      event.preventDefault();
      $("#modal-deactive").modal({
        show  : true,
        keyboard : false
      });
      $(".modal-backdrop").css( "z-index", "-1" );
      //$(  ).remove();
    }

    deactiveRecord(){
          this.lessonService.getById(this.idLesson)
                 .subscribe(
                    level =>  this.updateStatus(level),
                    error => console.log('Error: ' + error)
                 );
          //console.log(this.level);
          /*this.level.status = false;
          this.levelService.update(this.idLevel,this.level)
                 .subscribe(
                    result =>  {  
                    if ( result ) {
                        //this.msg = 'Se registro un nivel';
                        //$('#alert-success').show();
                    } else {
                        //this.msg = 'Ocurrio un error al registrar';
                        //$('#alert-danger').show();
                    }
          });  */     
    }

    updateStatus(level){
          ///console.log(level);
          //console.log(level.body);
          level.body.status = false;
          console.log(level.body.status);
          console.log(level.body);
         this.lessonService.update(this.idLesson,level.body)
             .subscribe(
                result =>  {  
                if ( result ) {
                    console.log(this.lessons);
                    //public remove(index: number): void {
                     this.lessons.splice(this.index, 1);
                    //}
                } else {
                    //this.msg = 'Ocurrio un error al registrar';
                    //$('#alert-danger').show();
                }
       })
    }

}
