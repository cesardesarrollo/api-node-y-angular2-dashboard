import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./deal.css');
const template  = require('./deal.html');

@Component({
    selector    : 'deal',
    template    : template,
    styles      : [ styles ]
})

export class Deal {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    editable: boolean;

    constructor(private router: Router, private http: Http){
        this.editable = false;
        this.title = 'Texto convenio';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }


}