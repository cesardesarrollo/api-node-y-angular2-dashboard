export class Deal {
  constructor(
  	public id: number,
    public status: boolean,
    public title: string,
    public type: string,
    public description: string,
    public createdAt: string,
    public updatedAt: string
  ) {  }
}