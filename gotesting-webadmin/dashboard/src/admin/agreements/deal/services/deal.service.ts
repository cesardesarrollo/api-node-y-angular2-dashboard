import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../../common/headers';
import { enviroment } 	  from '../../../../common/enviroments.ts';

const baseUrl = enviroment.base_url_api;

@Injectable()
export class DealService {


	constructor( private http: Http){}

	updateDeal(id,deal){
		let body = deal;
		return this.http
			.put(
				baseUrl + '/agreements/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			});
	}

	getDeal(){
         var filter = JSON.stringify({'where':{'type':'cdc'}});
        return this.http
            .get(
                baseUrl + '/agreements?filter='+filter,
                { headers : contentHeaders }
            )
            .map( (res) => {
                if( res.status === 200 ) {
                    return {status: true, body: res.json() };
                } else {
                    return {status: false, body: {}};
                }
            });
    
	}
}