import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../../common/headers';
import { enviroment } 	  from '../../../../common/enviroments.ts';

const baseUrl = enviroment.base_url_api;

@Injectable()
export class TermsService {

	constructor( private http: Http){}

	update(id,object){
		let body = JSON.stringify(object);
		let msg;
		return this.http
			.put(
				baseUrl + '/agreements/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}

			});
	}
	getTerms(id){
		let body = JSON.stringify({ id });
		let msg;
		return this.http
			.post(
				baseUrl + '/user/login',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.json().success ) {
					//localStorage.setItem('id_token', res.json().token );
					//msg = { success: res.json().success, userType : res.json().user_type, msg: res.json().msg };
				} else {
					//msg = { success: res.json().success, userType : res.json().user_type, msg: res.json().msg  };
				}
				//return msg;
			});
	}

	getById(id){
		let msg;
		return this.http
			.get(
				baseUrl + '/agreements/'+id,
				//body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				//console.log(res._body);
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
				//return msg;
			});
	}
}
