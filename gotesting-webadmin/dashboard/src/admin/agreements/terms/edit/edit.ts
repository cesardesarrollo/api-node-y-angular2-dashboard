import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { TermsService }                 from '../services/terms.service';
import { Term }                         from '../term.model';
import { Router, ActivatedRoute }       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-terms',
    template    : template,
    styles      : [ styles ],
    providers  : [ TermsService ]
})

export class TermsEdit implements OnInit {
    term:         Term;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    termForm: FormGroup;

    constructor(private route: ActivatedRoute, private fb: FormBuilder, private router: Router, private http: Http,private termsService: TermsService){
        window.scrollTo(0, 0);
        //default value because exist only one
        this.term = new Term(0,false,'','','','','');
        //this.title = 'Editar Términos y condiciones';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt); 
         
    }

    ngOnInit(){   

        this.route.params.subscribe(params => {
            if(params['id'] !== undefined) {
                this.getTerms(params['id']);
            }
        });

        this.termForm = this.fb.group({
            title: '',
            description: ''
        });

        $('#summernote').summernote({
            toolbar: [
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['style', ['bold', 'italic', 'underline', 'clear']]
      ]});
    }

    setTerms(terms){
        console.log(terms);
        this.termForm = this.fb.group(terms);
       $('#summernote').summernote('code',terms.description);
    }

    getTerms(id){
        this.termsService.getById(id)
            .subscribe(
                term =>  this.setTerms(term.body),
                error => console.log('Error: ' + error)
            );
    }

    onReturn(){
       this.router.navigate(['/administrator/agreements']);
    }

    onSubmit(){

       this.termForm.value.description = $('#summernote').summernote('code');
       if(this.termForm.value.description === ''){
            this.msg = 'Completa los campos';
            $('#alert-danger').show();
            setTimeout(function() {
                            $('#alert-danger').hide();       
            }, 2000);
           return;
       }      
       delete  this.termForm.value.updatedAt;
       this.termsService.update(this.termForm.value.id,this.termForm.value)
                 .subscribe(
                    result =>  {  
                    if ( result ) {
                        this.msg = 'Se editaron los acuerdos';
                        $('#alert-success').show();
                        setTimeout(function() {
                            $('#alert-success').hide();       
                        }, 2000);
                    } else {
                        this.msg = 'Ocurrio un error al editar';
                        $('#alert-danger').show();
                        setTimeout(function() {
                            $('#alert-danger').hide();       
                        }, 2000);
                    }       
                });
    }

}