import { routing } from './app.routes';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from 'angular2-jwt';
//pipes
import { DateHourPipe }  from './filters/date.pipe';
//services
import { LoginService }  from './login/login.service';
import { AuthService }   from './common/auth.service';
import { AuthGuard } 	  	from './common/auth.guard';
import { profileService } from './_tester/profile/profile.service';
import { AccountService } from './account/account.service';
import { AgreementService } from './admin/agreements/agreements.service';
//components
import { AppComponent }  from './app';
import { Login }  from './login/login';
import { Signup }  from './signup/signup';
import { Account }  from './account/account';
import { Home }  from './home/home';
import { Administrator }  from './administrator/administrator';
import { AdminHome }  from './admin/home/home';
import { Test }  from './admin/test/test';
import { Capacitation }  from './admin/capacitation/capacitation';
import { Certification }  from './admin/certification/certification';
import { Task }  from './admin/task/task';
import { Subscription }  from './admin/subscription/subscription';
import { Plan }  from './admin/plan/plan';
import { Levels, LevelsCreate, LevelsEdit }  from './admin/levels';
import { Lessons, LessonsCreate, LessonsEdit }  from './admin/lessons';
import { Platforms, PlatformsCreate, PlatformsEdit }  from './admin/platforms';
import { Courses, CoursesCreate, CoursesEdit }  from './admin/courses/';
import { Agreements }  from './admin/agreements';
import { Deal, DealEdit }  from './admin/agreements/deal';
import { Terms, TermsEdit }  from './admin/agreements/terms';
import { Tester } from './tester';
import { TesterHome } from './_tester/home';
import { Devices, DeviceNew } from './_tester/devices';
import { TCapacitation } from './_tester/capacitation';
import { TCertification } from './_tester/certification';
import { Profile } from './_tester/profile';
import { TTasks } from './_tester/tasks';

@NgModule({
  imports: [ BrowserModule, routing, ReactiveFormsModule, HttpModule, CommonModule ],       // module dependencies
  declarations: [ AppComponent, DateHourPipe, Login, Signup, Account, Home,
  				  Administrator, AdminHome, Test, Capacitation, Certification,
  				  Task, Subscription, Plan, Levels, LevelsCreate, LevelsEdit,
  				  Lessons, LessonsCreate, LessonsEdit, Platforms, PlatformsCreate, 
  				  PlatformsEdit, Courses, CoursesCreate, CoursesEdit, Deal,
  				  DealEdit, Terms, TermsEdit, Tester, TesterHome, Devices, 
  				  DeviceNew, TCapacitation, TCertification, Profile, TTasks,
  				  Agreements
  				 ],   // components and directives
  bootstrap: [ AppComponent ],     // root component
  providers: [ AuthGuard, LoginService, AuthService, profileService, AccountService,
  			   AgreementService
  			 ] // services
})

export class AppModule { }