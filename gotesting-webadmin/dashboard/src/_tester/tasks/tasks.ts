import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./tasks.css');
const template  = require('./tasks.html');

@Component({
    selector    : 'tasks',
    template    : template,
    styles      : [ styles ]
})

export class TTasks {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;

    constructor(private router: Router, private http: Http){
        this.title = 'Tasks view';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    logout(){
        localStorage.removeItem('id_token');
        this.router.navigate(['/login']);
    }
}