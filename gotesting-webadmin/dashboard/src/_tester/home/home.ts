import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./home.css');
const template  = require('./home.html');

@Component({
    selector    : 'tester-home',
    template    : template,
    styles      : [ styles ]
})

export class TesterHome implements OnInit {

    jwt:        string;
    decodedJwt : any;

	constructor(private router: Router){
		this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
	}

	ngOnInit(){
		console.log(this.decodedJwt);
		if(this.decodedJwt.user_firstLogin || this.decodedJwt.user_temporaryPassword){
			this.router.navigate(['/tester/profile'], {queryParams: {}});
		}
	}

}
