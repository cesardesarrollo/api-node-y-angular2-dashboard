import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { profileService } from './profile.service';

const styles    = require('./profile.css');
const template  = require('./profile.html');

@Component({
    selector    : 'profile',
    template    : template,
    styles      : [ styles ],
    providers   : [ profileService ]
})

export class Profile implements OnInit {
    msg:        string;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt : any;
    passForm: FormGroup;
    profileForm: FormGroup;

    constructor(private router: Router, private http: Http, private fb: FormBuilder, private ps: profileService){
        this.title = 'Profile view';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    ngOnInit(){
         this.profileForm = this.fb.group({
            name: '',
            lastname: '',
            maidenName: '',
            email: '',
            phone: '' 
        });
        this.passForm = this.fb.group({
            oldpass: '',
            pass: '',
            passSnd: '' 
        });
        this.getUserData(this.decodedJwt.user_id);
    }

   
    resetPass(){
        if(this.passForm.value.pass === this.passForm.value.passSnd){
            this.ps.resetPass(this.decodedJwt.user_id, this.passForm.value.oldpass, this.passForm.value.pass).subscribe(
                (result) => {
                console.log(result);
                    this.msg = result.body.message;
                    $('#alert-success2').show();
                    setTimeout(function(){
                        $('#alert-success2').hide();  
                    }, 3000);
                    this.ngOnInit();
            },
            (error) => {
                console.log(error.json());
                this.msg = error.json().error.message;
                $('#alert-danger2').show();
                setTimeout(function(){
                    $('#alert-danger2').hide();  
                }, 3000)
            }
            );
        }else{
            this.msg = 'Las contraseñas no coinciden intente de nuevo.';
            $('#alert-danger2').show();
            setTimeout(function(){
                    $('#alert-danger2').hide();  
            }, 3000)
        }
    }

    getUserData(id){    
        this.ps.getUser(id).subscribe(
            user => this.setUserData(user.body),
            error => console.log(error)
        )
    }

    setUserData(user){
        console.log(user);
        this.profileForm.patchValue({
            email: user.email !== null ? user.email  : '',
            name: user.name !== null ? user.name  : '',
            lastname: user.lastname !== null ? user.lastname  : '',
            maidenName: user.maidenName !== null ? user.maidenName  : '',
            phone: user.phone !== null ? user.phone  : ''
        });
    }

    saveProfile(){
        this.ps.updateUser(this.decodedJwt.user_id,this.profileForm.value).subscribe(
           (res) => {
                this.msg = 'Tus datos fueron actualizados exitosamente.';
                console.log("ssisis");
                $('#alert-success').show();
                setTimeout(function(){
                        $('#alert-success').hide();  
                    }, 3000)}
           ,
           (error) => console.log(error)
        );
    }

    uploadImage(){
        $("#upload-image").click();
    }

    uploadFile(){
        $("#upload-file").click();
    }

    logout(){
        //localStorage.removeItem('id_token');
        this.router.navigate(['/login']);
    }
}
