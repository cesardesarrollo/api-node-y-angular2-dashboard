import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../common/headers';
import { enviroment } 	  from '../../common/enviroments.ts';
import { Observable }     from 'rxjs/Rx';

const baseUrl = enviroment.base_url_api;

@Injectable()
export class profileService {

	constructor( private http: Http){}

	resetPass(id,oldPass,newPass){
		//console.log(newPass);
		let body = JSON.stringify({ old_password: oldPass, new_password: newPass });
		let msg;
		return this.http
			.post(
				baseUrl + '/users/'+id+'/change/password',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					//return res.json().response;
					return {status: true, body: res.json().response };
				} else {
					return {status: true, body: res.json().response };
				}
			})
			.catch(this.handleError);
	}

	updateUser(id,object){
		return this.http
			.put(
				baseUrl + '/users/'+id,
				object,
				{ headers : contentHeaders }
			)
			.map( (res) => {

				if( res.status === 200 ) {
					return res;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}

	getUser(id){
		let msg;
		return this.http
		.get(
			baseUrl + '/users/'+id,
			{ headers : contentHeaders }
		)
		.map( (res) => {
			if( res.status === 200 ) {
				return {status: true, body: res.json() };
			} else {
				return {status: false, body: {}};
			}
		});
	}
	
	private handleError (error: any) {
		//console.log(error);
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}

}