import { Component }                         from '@angular/core';
import { Http, Headers }                     from '@angular/http';
import { contentHeaders }                    from '../common/headers';
import { AccountService }                    from './account.service.ts';
import { Router,ActivatedRoute }                            from '@angular/router';

const styles   = require('./account.css');
const template = require('./account.html');

@Component({
  selector   : 'account',
  template   : template,
  styles     : [ styles ],
  providers  : [ AccountService ]
})

export class Account {
  
  msg: string;
  msgPassword: string;

  constructor(private router: Router, private route: ActivatedRoute, private accountService: AccountService) { }
  
  ngOnInit(){
    let route = this.router;
    this.msg = 'Verificando...';
    this.route.params.subscribe( params => {
      let c = params['c'];
      let e = params['e'];

      this.accountService.activate(e, c)
        .subscribe( res =>{
            console.log( res );
            if( res.success ){
                this.msg = res.msg;
            } 
          },
            error => this.msg = error.json().error.message
        );
    });      
  }

}