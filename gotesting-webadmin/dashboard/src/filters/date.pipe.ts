import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'dateHour', pure: true})
export class DateHourPipe implements PipeTransform {
  transform(value: string, args: string[]): any { 
    if (!value) return value;
    var strIndexDate = value.indexOf("T");
    var date = value.substring(0, strIndexDate);
    console.log(date);
    var hour = value.substring(strIndexDate+1, strIndexDate+9);
    console.log(hour);
    return "Dia: "+date+" Hora: "+hour;
  }
}