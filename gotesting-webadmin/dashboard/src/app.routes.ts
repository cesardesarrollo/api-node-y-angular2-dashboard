import { RouterModule, Routes } from '@angular/router';
import { Home } 		    from './home';
import { Login } 		    from './login';
import { Signup } 		  from './signup';
/*
 *  ADMINISTRATOR ROUTES
 *  =======================
 */
import { Administrator }from './administrator';
import { Test }         from './admin/test';
import { Capacitation } from './admin/capacitation';
import { Certification }from './admin/certification';
import { Plan }         from './admin/plan';
import { Levels }       from './admin/levels';
import { LevelsCreate } from './admin/levels/create';
import { LevelsEdit }   from './admin/levels/edit';
import { Lessons }      from './admin/lessons';
import { LessonsCreate }from './admin/lessons/create';
import { LessonsEdit }  from './admin/lessons/edit';
import { Platforms }    from './admin/platforms';
import { PlatformsCreate } from './admin/platforms/create';
import { PlatformsEdit }from './admin/platforms/edit';
import { Courses }      from './admin/courses';
import { CoursesCreate }      from './admin/courses/create';
import { CoursesEdit }      from './admin/courses/edit';
import { Agreements }     from './admin/agreements';
import { Deal }         from './admin/agreements/deal';
import { DealEdit }     from './admin/agreements/deal/edit';
import { Terms }        from './admin/agreements/terms';
import { TermsEdit }    from './admin/agreements/terms/edit';

//import { ATerms }        from '.admin/settings/terms';
import { Task }         from './admin/task';
import { Subscription } from './admin/subscription';
import { AdminHome }    from './admin/home';
import { Account }      from './account';
import { Tester }       from './tester';
/*
 *  TESTER ROUTES
 *  =======================
 */
import { Devices }      from './_tester/devices';
import { DeviceNew }    from './_tester/devices';
import { TCapacitation }from './_tester/capacitation';
import { TCertification}from './_tester/certification';
import { Profile }      from './_tester/profile';
import { TTasks }       from './_tester/tasks';
import { TesterHome }   from './_tester/home';

import { AuthGuard } 	  from './common/auth.guard';

const routes: Routes = [
  { path: '',       component:  Login },
  { path: 'login',  component: Login },
  { path: 'signup', component: Signup },
  { path: 'account/activation/:e/:c',  component: Account },  
  { path: 'home',   component: Home/*, canActivate: [AuthGuard]*/ },

  { path: 'administrator',   component: Administrator, /*canActivate: [AuthGuard],*/ 
    children: [
     { path: '', redirectTo: 'home' },
     { path: 'home', component: AdminHome },
     { path: 'test', component: Test }, 
     { path: 'capacitation', component: Capacitation },
     { path: 'certification', component: Certification },
     { path: 'task', component: Task } ,
     { path: 'subscription', component: Subscription },
     { path: 'plans', component: Plan },
     { path: 'levels', component: Levels },
     { path: 'levels/create', component: LevelsCreate },
     { path: 'levels/edit/:id', component: LevelsEdit },
     { path: 'lessons', component: Lessons },
     { path: 'lessons/create', component: LessonsCreate },
     { path: 'lessons/edit/:id', component: LessonsEdit },
     { path: 'platforms', component: Platforms },
     { path: 'platforms/create', component: PlatformsCreate },
     { path: 'platforms/edit/:id', component: PlatformsEdit },
     { path: 'courses', component: Courses },
     { path: 'courses/create', component: CoursesCreate },
     { path: 'courses/edit', component: CoursesEdit },
     { path: 'agreements', component: Agreements },
     { path: 'agreements/edit/:id', component: TermsEdit },
     //{ path: 'agreements/deal', component: Deal},
     //{ path: 'agreements/deal/edit', component: DealEdit},
     //{ path: 'agreements/terms', component: Terms},
     //{ path: 'agreements/terms/edit', component: TermsEdit}
    ]
  },

   /*  TESTER ROUTES
   *  ================ 
   */
  { path: 'tester', component: Tester, 
    children: [
      { path: '', redirectTo: 'home'},
      { path: 'home', component: TesterHome },
      { path: 'devices', component: Devices },
      { path: 'devices/new', component: DeviceNew},
      { path: 'capacitation', component: TCapacitation},
      { path: 'certification', component: TCertification},
      { path: 'profile', component: Profile},
      { path: 'tasks', component: TTasks}
    ]
  },
  { path: '**',     component: Login }
];

export const routing = RouterModule.forRoot(routes);