import { Component }        from '@angular/core';
import { Http, Headers}     from '@angular/http';
import { AuthHttp }         from 'angular2-jwt';
import { Router }           from '@angular/router';

const styles    = require('./tester.css');
const template  = require('./tester.html');

@Component({
    selector    : 'tester',
    template    : template,
    styles      : [ styles ]
})

export class Tester {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;

    constructor(private router: Router, private http: Http){
        this.title = 'Welcome Tester';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    logout(){
        localStorage.removeItem('id_token');
        this.router.navigate(['/login']);
    }
}