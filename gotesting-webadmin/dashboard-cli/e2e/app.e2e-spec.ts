import { DashboardCliPage } from './app.po';

describe('dashboard-cli App', function() {
  let page: DashboardCliPage;

  beforeEach(() => {
    page = new DashboardCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
