import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }          from 'angular2-jwt';
import { Router }                       from '@angular/router';
import { deviceService }                from './services/device.services';

declare var $:any;

const styles    = require('./devices.css');
const template  = require('./devices.html');

@Component({
    selector    : 'tester-devices',
    template    : template,
    styles      : [ styles ],
    providers   : [ deviceService ]
})

export class Devices {
    viewTitle:      string;
    userName:       string;
    userRole:       string;
    jwt:            string;
    decodedJwt:     any;
    devices:        any [];
    userId:         number;
    deviceEmpty:    Boolean;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router, private http: Http, private ds: deviceService){
        this.viewTitle = 'Mis dispositivos';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.userId = this.decodedJwt.user_id;
        this.deviceEmpty = true;
    }
    
    ngOnInit(){
        $("#tester__nav li a").removeClass('active');
        $("#devices_nav a").addClass('active');
        $('html, body').animate({scrollTop : 0},800);
        this.getTesterDevices();
    }
    
    //GET USER DEVICES
    private getTesterDevices(){
        console.log( this.userId);
        this.ds
        .getAll(this.userId)
        .subscribe( devices => { 
            this.devices = devices
            if( this.devices.length > 0){
                this.deviceEmpty = false;
            }
            console.log( this.devices ); 
        });
    }

}