import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { environment } 	  from '../../../../environments/environment';
import { Observable }     from 'rxjs/Rx';

const baseUrl = environment.base_url_api;

@Injectable()
export class deviceService {

	constructor( private http: Http){}

   	/**
	 @GET ALL DEVICES BY TESTER
	**/
	getAll(tester_id){
		//let filter = JSON.stringify({'where' :{'tester_Id' : 3}});
		let filter = JSON.stringify({
				"where": {"tester_Id":tester_id},
				"include": { "relation":"device_Id", "scope": { "fields": ["name","dev_producer_Id","dev_category_Id", "dev_platform_Id"], 
					"include":[ { "relation":"producer_id"}, { "relation":"category_id"}, { "relation":"platform_id"} ] } 
			}
		});
		return this.http
			.get( baseUrl + '/tester_has_devices?filter=' + filter, {headers: contentHeaders})
			.map( (devices) => devices.json())
			.catch( this.handleError );
	}

	/**
	 @GET ALL PLATFORMS
	**/
	getPlatforms(){
		return this.http
			.get( baseUrl + '/platforms', {headers: contentHeaders})
			.map( (platforms ) => platforms.json() )
			.catch( this.handleError );
	}

	/**
	 @GET ALL CATEGORIES
	**/
	getCategories(){
		return this.http
			.get( baseUrl + '/categories', {headers: contentHeaders})
			.map( (categories)=> categories.json())
			.catch( this.handleError );
	}

	/**
	 @GET ALL PRODUCERS
	**/
	getProducers(){
		return this.http
			.get( baseUrl + '/producers', {headers: contentHeaders})
			.map( (producers)=> producers.json())
			.catch( this.handleError );
	}

	/**
	 @GET DEVICES BY CATEGORIES
	**/
	getDevicesByOptions(){
		return this.http
			.get( baseUrl + '/devices', {headers: contentHeaders})
			.map( (devices)=> devices.json())
			.catch(this.handleError);
	}

	/**
	 @GET DEVICES BY FEATURES
	**/
	getDevicesByFeatures(producer_id){
		let filter = JSON.stringify({'where' :{'dev_producer_Id' : producer_id}});
		return this.http
			.get( baseUrl + '/devices?filter=' + filter, {headers: contentHeaders})
			.map( (devices) => devices.json())
			.catch(this.handleError);
	}

	/**
	 @GET DEVICE BY ID
	**/
	getDeviceById(device_id){
		let filter = JSON.stringify({"include":[ 
			{"relation":"category_id","scope":{"fields":{"name":"true", "description": "true", "type": "true"}}}, 
			{"relation":"platform_id","scope":{"fields":{"name":"true", "description": "true"}}}, 
			{"relation":"producer_id","scope":{"fields":{"name":"true", "description": "true"}}} 
		]});
		return this.http
			.get( baseUrl + '/devices/' + device_id + '?filter=' + filter, {headers: contentHeaders})
			.map( (device) => device.json())
			.catch(this.handleError);
	}

	/**
	 @CREATE NEW DEVICE FOR TESTER 
	**/
	create( _device ) {
		return this.http
			.post( baseUrl + '/tester_has_devices', _device, { headers : contentHeaders })
			.map( (device) => { device.json() })
			.catch(this.handleError);
	}

	//MANAGE ERRORS
	private handleError (error: any) {
		//console.log(error);
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}

}