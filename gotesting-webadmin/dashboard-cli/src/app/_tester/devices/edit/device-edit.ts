import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                        from '@angular/http';
import { AuthHttp, JwtHelper }                             from 'angular2-jwt';
import { Router, ActivatedRoute  }              from '@angular/router';
import { deviceService }                        from '../services/device.services';
import { FormBuilder, FormGroup, FormControl }  from '@angular/forms';

declare var $:any;

const styles    = require('./device-edit.css');
const template  = require('./device-edit.html');

@Component({
    selector    : 'device-edit',
    template    : template,
    styles      : [ styles ],
    providers   : [ deviceService ]
})

export class EditDevice implements OnInit {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: any;
    deviceCategory: string;
    devicePlatform: string;
    deviceImage:    string;
    deviceId:       number;

    //<Lists>
    platforms:  any;
    producers:  any;
    categories: any;
    devices:    any;
    device:     any;

    //Forms
    deviceForm: FormGroup;
    //category: string;
    model  = { options: 'smartphone' };
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router, private http: Http, private deviceService: deviceService,private fb: FormBuilder, private route: ActivatedRoute){
        this.title = 'Devices view';
        // this.producer = '';
        // this.category = '';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        $('html, body').animate({scrollTop : 0},2000);
    }

    ngOnInit(){
        $("#tester__nav li a").removeClass('active');
        $("#devices_nav a").addClass('active');
        this.deviceForm = this.fb.group({ ram: '', storage: '', code: '' });
        this.route.params.subscribe( params => { this.deviceId = params['id']; });
        this.getDeviceById()

    }
    /**
     @EDIT DEVICE
    **/
    editDevice(){
        let self = this;
        let _device = this.deviceForm.value;
        _device['tester_Id'] = this.decodedJwt.user_id;
        _device['device_Id'] = parseInt( $("#cmbDevice").val() );

        this.deviceService.create( JSON.stringify(_device) ).subscribe(
            device => { 
                if( _device ){
                    $("#alertify").addClass('success active');
                    $('#alertify div.icon').html('<i class="fa fa-check"></i>');
                    $('#alertify div.msg span').html('Tus datos fueron agregados exitosamente.');
                    setTimeout(function(){
                        $("#alertify").removeClass('active success');
                        self.router.navigate(['/tester/devices']);
                    }, 3500);
                } 
            }
        )

    }

    /**
     @GET ALL PRODUCERS
    **/
    private getProducers(){
        this.deviceService
        .getProducers()
        .subscribe( producers => { this.producers = producers });
    }

    /**
     @ONCHANGE PRODUCER
    **/
    onProducerChange(producer_id){
        this.deviceService.getDevicesByFeatures(producer_id).subscribe(
            devices => { this.devices = devices }
        );
    }

    /**
     @ONCHANGE DEVICE
    **/
    onDeviceChange(device_id){
        this.deviceService.getDeviceById(device_id).subscribe(
            device => { 
                this.device = device;
                if( this.device.category_id.id === 1)
                    this.deviceImage = 'desktop_mac';
                else if( this.device.category_id.id === 2)
                    this.deviceImage = 'tablet_mac';
                else if( this.device.category_id.id === 3)
                    this.deviceImage = 'smartphone';
                this.deviceCategory = this.device.category_id.name;
                this.devicePlatform = this.device.platform_id.name;
            }
        )

    }
    /**
     @VALIDATE MAC ADDRESS
    **/
    formatMAC(e){
        let r   = /([a-f0-9]{2})([a-f0-9]{2})/i,
            str = e.target.value.replace(/[^a-f0-9]/ig, "");
        
        while (r.test(str)) {
            str = str.replace(r, '$1' + ':' + '$2');
        }

        e.target.value = str.slice(0, 17);
    }

    /**
     @GET DEVICE BY ID
    **/
    getDeviceById(){
        this.deviceService.getDeviceById(this.deviceId).subscribe(
            device => { 
                this.device = device;
                this.deviceImage    = this.device.category_id.type;
                this.deviceCategory = "Categoría: " + this.device.category_id.name;
                this.devicePlatform = "Plataforma: " + this.device.platform_id.name;


                
                console.log( this.device );
            }
        )

    }
}