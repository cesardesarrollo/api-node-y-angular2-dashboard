import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }          from 'angular2-jwt';
import { Router }                       from '@angular/router';

declare var $:any;

const styles    = require('./finantial.css');
const template  = require('./finantial.html');

@Component({
    selector    : 'tester-finantial',
    template    : template,
    styles      : [ styles ]
})

export class TFinantial {
    viewTitle:      string;
    jwt:            string;
    decodedJwt:     any;
    devices:        any [];
    userId:         number;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router, private http: Http){
        this.viewTitle = 'Mis tarjetas';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.userId = this.decodedJwt.user_id;
    }
    
    ngOnInit(){
        $("#tester__nav li a").removeClass('active');
        $("#banking_nav a").addClass('active');
        $('html, body').animate({scrollTop : 0},800);
    }
}