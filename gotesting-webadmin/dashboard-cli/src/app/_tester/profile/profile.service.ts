import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../common/headers';
import { environment } 	  from '../../../environments/environment';
import { AuthService } 	  from '../../common/auth.service';
import { Observable }     from 'rxjs/Rx';

const baseUrl = environment.base_url_api;

@Injectable()
export class profileService {
	
	constructor( private http: Http, private as: AuthService){}

	/**
     @RESET PASSWORD
     @PARAMS: { oldPassword, newPassword }
    **/   
	resetPass(id,oldPass,newPass){
		let headers = contentHeaders;
		headers.append('token', this.as.getUserToken());
		let body = JSON.stringify({ old_password: oldPass, new_password: newPass });
		let msg;
		return this.http
			.post( baseUrl + '/users/'+id+'/change/password', body, { headers: contentHeaders })
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json().response };
				} else {
					return {status: true, body: res.json().response };
				}
			})
			.catch(this.handleError);
	}

	/**
     @UPDATE PROFILE DATA
     @PARAMS : { user_id, _dataForm } 
    **/
	updateUser(object){
		let headers = contentHeaders;
		headers.append('token', this.as.getUserToken());
		return this.http
			.put( baseUrl + '/users/update/profile', object, { headers: headers })
			.map( (res) => {
				if( res.status === 200 ) {
					return res;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}
	/**
     @GET USER DATA
     @PARAMS : { user_id } 
    **/
	getUser(id){
		let msg;
		return this.http
		.get(
			baseUrl + '/users/'+id,
			{ headers : contentHeaders }
		)
		.map( (res) => {
			if( res.status === 200 ) {
				return {status: true, body: res.json() };
			} else {
				return {status: false, body: {}};
			}
		});
	}

	/**
	 @UPLOAD OFFICIAL ID 
	**/
	uploadId(official_id){
		let headers = contentHeaders;
		headers.append('token', this.as.getUserToken());
		let _data = { 'official_id': official_id };
		return this.http
			.put( baseUrl + '/users/update/official_id', _data, {headers: headers})
			.map( (response) => { return response; })
			.catch( this.handleError );
	}
	
	private handleError (error: any) {
		//console.log(error);
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}

}