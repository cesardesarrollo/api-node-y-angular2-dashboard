import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                        from '@angular/http';
import { AuthHttp,JwtHelper }                   from 'angular2-jwt';
import { Router }                               from '@angular/router';
import { FormBuilder, FormGroup, FormControl }  from '@angular/forms';
import { profileService }                       from './profile.service';

declare var $:any;

const styles    = require('./profile.css');
const template  = require('./profile.html');

@Component({
    selector    : 'tester-profile',
    template    : template,
    styles      : [ styles ],
    providers   : [ profileService ]
})

export class Profile implements OnInit {
    msg:            string;
    viewTitle:      string;
    userName:       string;
    userRole:       string;
    jwt:            string;
    decodedJwt:     any;
    passForm:       FormGroup;
    profileForm:    FormGroup;
    jwtHelper:      JwtHelper = new JwtHelper();
    testerAvatar:   string;
    testerEmail:    string;
    
    constructor(private router: Router, private http: Http, private fb: FormBuilder, private ps: profileService){
        this.viewTitle = 'Mi perfil';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
        $("#tester__nav li a").removeClass('active');
        $("#profile_nav a").addClass('active');
        this.profileForm = this.fb.group({name: '', lastname: '', maidenName: '', email: '', phone: ''});
        this.passForm = this.fb.group({oldpass: '', pass: '', passSnd: ''});
        this.getUserData(this.decodedJwt.user_id);
    }

    
    /**
     @GET USER DATA
    **/
    getUserData(id){    
        this.ps.getUser(id).subscribe(
            user => this.setUserData(user.body),
            error => console.log(error)
        )
    }

    /**
     @SET USER DATA
     @PARAMS: { userObject }
    **/
    setUserData(user){
        this.testerAvatar = user.avatar;
        this.testerEmail  = user.email;
        this.profileForm.patchValue({ 
            email: user.email !== null ? user.email  : '',
            name: user.name !== null ? user.name  : '',
            lastname: user.lastname !== null ? user.lastname  : '',
            maidenName: user.maidenName !== null ? user.maidenName  : '',
            phone: user.phone !== null ? user.phone  : ''
        });
    }

    /**
     @IMAGE PREVIER LOADER
    **/
    uploadImage(event){
        if( event.srcElement.files && event.srcElement.files[0] ){
            var reader = new FileReader();
            reader.onload = function(e){
                $(".avatar-preview img").attr('src', e.target.result );
                $(".Tester__profile--img img").attr('src', e.target.result);
                $(".avatar-preview img").attr('data-image', e.target.result );
            }
            reader.readAsDataURL( event.srcElement.files[0] );
        } else {
            console.log('No carga la imagen');
        }        
    }

    /**
     @UPDATE PROFILE DATA
     @PARAMS : { user_id, _dataForm } 
    **/
    saveProfile(){
        let self = this;
        let _data = this.profileForm.value;
        _data['avatar'] = $(".avatar-preview img").attr('data-image') || 'avatar_default.png';
        this.ps.updateUser(_data).subscribe(
           (res) => {
                $("#alertify").addClass('success active');
                $('#alertify div.icon').html('<i class="fa fa-check"></i>');
                $('#alertify div.msg span').html('Tus datos fueron actualizados exitosamente.');
                setTimeout(function(){
                    $("#alertify").removeClass('active success');
                    self.ngOnInit();
                }, 3500);
           },
           (error) => console.log(error)
        );
    }

    /**
     @RESET PASSWORD
     @PARAMS: { oldPassword, newPassword }
    **/    
    resetPass(){
        let self = this;
        if(this.passForm.value.pass === this.passForm.value.passSnd){

            this.ps.resetPass(this.decodedJwt.user_id, this.passForm.value.oldpass, this.passForm.value.pass).subscribe(
                (result) => {
                    $("#alertify").addClass('success active');
                    $('#alertify div.icon').html('<i class="fa fa-check"></i>');
                    $('#alertify div.msg span').html(result.body.message);
                    setTimeout(function(){
                        $("#alertify").removeClass('active success');
                    }, 3500);
                    self.ngOnInit();
                },
                (error) => {
                    $("#alertify").addClass('danger active');
                    $('#alertify div.icon').html('<i class="fa fa-check"></i>');
                    $('#alertify div.msg span').html(error.json().error.message);
                    setTimeout(function(){
                        $("#alertify").removeClass('active success');
                    }, 3500);
                }
            );

        } else {
            $("#alertify").addClass('danger active');
            $('#alertify div.icon').html('<i class="fa fa-check"></i>');
            $('#alertify div.msg span').html('Las contraseñas no coinciden intente de nuevo.');
            setTimeout(function(){
                $("#alertify").removeClass('active success');
            }, 3500);
        }
    }

    /**
     @UPLOAD OFFICIAL ID 
    **/
    uploadId(event){
        var self = this;
        if( event.srcElement.files && event.srcElement.files[0] ){
            var reader = new FileReader();
            reader.onload = function(e){
                $(".id-preview img").attr('src', e.target.result );
                $(".id-preview img").attr('data-image', e.target.result );
                self.ps.uploadId( $(".id-preview img").attr('data-image', e.target.result ) ).subscribe( (response) =>{
                    console.log( response );
                })
            }
            reader.readAsDataURL( event.srcElement.files[0] );
        } else {
            console.log('No carga la imagen');
        }        
    }
}
