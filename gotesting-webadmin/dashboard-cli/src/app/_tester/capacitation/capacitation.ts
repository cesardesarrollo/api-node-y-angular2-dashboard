import { Component, NgModule }                  from '@angular/core';
import { Http, Headers }                        from '@angular/http';
import { AuthHttp, JwtHelper }                  from 'angular2-jwt';
import { Router }                               from '@angular/router';
import { FormBuilder, FormGroup, FormControl }  from '@angular/forms';
import { capacitationService }                  from './services/capacitation.service';


const styles    = require('./capacitation.css');
const template  = require('./capacitation.html');

declare var $:any;

@Component({
    selector    : 'tester-capacitation',
    template    : template,
    styles      : [ styles ],
    providers   : [ capacitationService ]
})


export class TCapacitation {
    viewTitle:  string;
    username:   string;
    userid:     number;
    userRole:   string;
    jwt:        string;
    decodedJwt: any;
    searchCourseForm: FormGroup;
    courses: any [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router, private http: Http, private fb: FormBuilder, private cs: capacitationService){
        this.viewTitle = 'Cursos';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
        $("#tester__nav li a").removeClass('active');
        $("#capacitation_nav a").addClass('active');
        this.searchCourseForm = this.fb.group({courseName: ''});
        $('html, body').animate({scrollTop : 0},800);
        this.username = this.decodedJwt.user_email.split('@')[0];
        this.userid = this.decodedJwt.user_id;
        this.getUserCourses();
    }

    private getUserCourses(){
        this.cs.getCourses().subscribe( courses => { this.courses = courses });
    }

    /**
     @GET COURSES
     @PARAM { course_id }
    **/
    getCourse(course){
        console.log(course);
        let self = this;
        this.cs.setCourseTester(course.id).subscribe( course => {
            if( course ){
                $("#alertify").addClass('success active');
                $('#alertify div.icon').html('<i class="fa fa-check"></i>');
                $('#alertify div.msg span').html('Se ha agregado este curso a tu lista.');
                setTimeout(function(){
                    $("#alertify").removeClass('active success');
                }, 3500);
            } 
        })
    }
}