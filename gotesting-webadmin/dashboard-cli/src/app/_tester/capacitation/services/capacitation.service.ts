import { Injectable }       from '@angular/core';
import { Http }             from '@angular/http';
import { contentHeaders }   from '../../../common/headers';
import { environment }      from '../../../../environments/environment';
import { Observable }       from 'rxjs/Rx';
import { AuthService } 	    from '../../../common/auth.service';

const baseUrl = environment.base_url_api;

@Injectable()
export class capacitationService {

    //BUILD CLASS CONSTRUCTOR
    constructor(private http: Http, private as: AuthService){}

    getCourses(){
        let msg;
        let user_level = 1;
        let filter = JSON.stringify({'where' : {'levels_Id' : user_level }});
        return this.http
            .get( baseUrl + '/courses', { headers : contentHeaders })
            .map( (courses) =>  courses.json())
            .catch( this.handleError );
    }

    /**
     @GET COURSE DETAIL
     @PARAMS { course_id }
    **/
    courseDetail(course_id){
        return this.http
            .get( baseUrl + '/courses/' + course_id, {headers: contentHeaders})
            .map( (course)=> course.json())
            .catch( this.handleError );
    }

    /**
     @ADD COURSE TO TESTER
     @PARAMS { course_id, tester_id }
    **/
    setCourseTester(course){
        let _data = JSON.stringify({ "idCourse": course});
        let headers = contentHeaders;
		headers.append('token', this.as.getUserToken());
        return this.http
            .post( baseUrl + '/testers/setCourse', _data, {headers: headers})
            .map( (course) => course.json())
            .catch( this.handleError );
    }

    /**
     @GET COURSES BY USER
     @PARAMS { tester_id }
    **/
    getCoursesByTester(){
		let headers = contentHeaders;
		headers.append('token', this.as.getUserToken());
        return this.http
            .get( baseUrl + '/testers/courses', {headers: headers})
            .map( (courses)=> courses.json())
            .catch( this.handleError );
    }

    /**
     @GET LESSON DETAIL
    **/
    getLessonDetail(lesson_id){
        return this.http
            .get( baseUrl + '/lessons/' + lesson_id +'/files', {headers: contentHeaders})
            .map( (lesson) => lesson.json())
            .catch( this.handleError );
    }

    //HANDLE ERROR
    private handleError (error: any) {
		//console.log(error);
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}


}