import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router, ActivatedRoute }       from '@angular/router';
import { capacitationService }          from '../services/capacitation.service';

const styles    = require('./course-detail.css');
const template  = require('./course-detail.html');
declare var $:any;

@Component({
    selector    : 'course-detail',
    template    : template,
    styles      : [ styles ],
    providers   : [capacitationService]
})

export class TCourseDetail implements OnInit {
    titleView:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    course_id:  number;
    course_detail: any [];
    lessons_detail: any [];
    lesson_media: any[];
    lessonMedia: any[];

    constructor(private router: Router, private http: Http, private route: ActivatedRoute, private courseService: capacitationService){
        this.titleView = 'Course detail view';
        this.jwt = localStorage.getItem('id_token');
        //this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    ngOnInit(){
        $("#tester__nav li a").removeClass('active');
        $("#capacitation_nav a").addClass('active');
        this.route.params.subscribe( params => {this.course_id = params['id']});
        this.getCourseDetail();
    }


    /**
     @GET COURSE detail
     @PARAMS { course_id }
    **/
    private getCourseDetail(){
        this.courseService
            .courseDetail( this.course_id )
            .subscribe( (course)=> {
                    console.log( course );
                    this.course_detail = course.response.course;
                    this.lessons_detail = course.response.lessons;
                    //this.lessonMedia = course.response.lessons[0][0].lesson_data.urlArr.split(',');
                }
            );
    }

    /**
     @LOAD VIDEO TO PREVIEW
    **/
    loadLessonVideo(event){
        let el = event.target;
        $("#lesson-video-player").attr('src', "http://www.youtube.com/embed/" + $(el).attr('data-lesson-video') + "?enablejsapi=1&");
    }
}