import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router, ActivatedRoute }       from '@angular/router';
import { capacitationService }          from '../services/capacitation.service';

const styles    = require('./my-courses.css');
const template  = require('./my-courses.html');
declare var $:any;

@Component({
    selector    : 'my-courses',
    template    : template,
    styles      : [ styles ],
    providers   : [capacitationService]
})

export class MyCourses implements OnInit {
    titleView:  string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    course_id:  number;
    course_detail: any [];
    lessons_detail: any [];
    emptyCourses: boolean;
    courses: any;

    constructor(private router: Router, private http: Http, private route: ActivatedRoute, private courseService: capacitationService){
        this.titleView = 'Course detail view';
        this.jwt = localStorage.getItem('id_token');
        //this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    ngOnInit(){
        $("#tester__nav li a").removeClass('active');
        $("#capacitation_nav a").addClass('active');
        this.emptyCourses = true;
        this.getTesterCourses();
    }


    /**
     @GET TESTER COURSES
    **/
    getTesterCourses(){
        this.courseService.getCoursesByTester().subscribe((courses)=>{
            if( courses.response.length === 0)
                this.emptyCourses = true;
            else{
                this.emptyCourses = false;
                this.courses = courses.response;
            }
        })
    }
    
}