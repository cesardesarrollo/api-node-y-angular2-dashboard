import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router, ActivatedRoute }       from '@angular/router';
import { capacitationService }          from '../services/capacitation.service';

const styles    = require('./lesson-detail.css');
const template  = require('./lesson-detail.html');
declare var $:any;

@Component({
    selector    : 'lesson-detail',
    template    : template,
    styles      : [ styles ],
    providers   : [capacitationService]
})

export class TLessonDetail implements OnInit {
    titleView:      string;
    userName:       string;
    userRole:       string;
    jwt:            string;
    decodedJwt:     string;
    course_id:      number;
    lesson_id:      number;
    course_detail:  any [];
    lesson_detail:  any [];
    lesson_media:   any[];
    lessonMedia:    any[];

    constructor(private router: Router, private http: Http, private route: ActivatedRoute, private courseService: capacitationService){
        this.titleView = 'Course detail view';
        this.jwt = localStorage.getItem('id_token');
        //this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    ngOnInit(){
        $("#tester__nav li a").removeClass('active');
        $("#capacitation_nav a").addClass('active');
        this.route.params.subscribe( params => {this.course_id = params['course_id']; this.lesson_id = params['lesson_id']});
        this.getLessonDetail();
    }

    /**
     @GETLESSON DETAIL
    **/
    getLessonDetail(){
        this.courseService.getLessonDetail(this.lesson_id).subscribe( (lesson) => {
                this.lesson_detail = lesson.response;
                this.lesson_media = lesson.response.urlArr.split(','); 
            }
        )
    }

    /**
     @LOAD VIDEO TO PREVIEW
    **/
    loadLessonVideo(event){
        event.preventDefault();
        let el = event.target;
        $("#lesson-video-player").attr('src', "http://www.youtube.com/embed/" + $(el).attr('data-lesson-video') + "?enablejsapi=1&");
        $(el).addClass('active-video');
    }
}