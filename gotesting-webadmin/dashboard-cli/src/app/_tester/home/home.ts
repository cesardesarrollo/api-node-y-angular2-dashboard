import { Component, OnInit }    from '@angular/core';
import { Http, Headers }        from '@angular/http';
import { AuthHttp, JwtHelper }  from 'angular2-jwt';
import { Router }               from '@angular/router';
import { testerService }		from '../../tester/tester.service';

declare var $:any;

const styles    = require('./home.css');
const template  = require('./home.html');

@Component({
    selector    : 'tester-home',
    template    : template,
    styles      : [ styles ]
})

export class TesterHome implements OnInit {

    jwt:        string;
    decodedJwt:	any;
    jwtHelper:	JwtHelper = new JwtHelper();
	user: 		any [];

	constructor(private router: Router, private ts: testerService){
		this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
	}

	ngOnInit(){
		$("#tester__nav li a").removeClass('active');
        $("#home_nav a").addClass('active');
		this.getDataTester();
		if(this.decodedJwt.user_firstLogin || this.decodedJwt.user_temporaryPassword){
		 	this.router.navigate(['/tester/profile'], {queryParams: {}});
		}
	}

	private getDataTester(){
		this.ts.getTester(this.decodedJwt.user_id).subscribe( user => { 
            this.user = user
            this.jwt  = user.token;
            this.decodedJwt = this.jwtHelper.decodeToken( this.jwt ); 
        });
	}

}
