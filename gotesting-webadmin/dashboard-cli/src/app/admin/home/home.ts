import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

declare var $:any;

const styles    = require('./home.css');
const template  = require('./home.html');

@Component({
    selector    : 'admin-home',
    template    : template,
    styles      : [ styles ]
})

export class AdminHome {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor( private router: Router, private http: Http ) { 
        this.title = 'Bienvenido';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    logout(){
        localStorage.removeItem('id_token');
        this.router.navigate(['/login']);
    }

    ngOnInit(){
        $('#menu-trigger').toggleClass('open');
        $('#sidebar').toggleClass('toggled');
    }

}
