export class Task {
  constructor(
  	public id: number,
    public title: string,
    public description: string,
    public start_date: string,
    public end_date: string,
    public cost: number,
    public state: string,
    public apply: string,
    public status: boolean,
    public matriz_test_Id: number,
    public template_rin_Id: number,
    public platform_id: number
  ) {  }
}