import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }			from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./task.css');
const template  = require('./task.html');

@Component({
    selector    : 'task',
    template    : template,
    styles      : [ styles ]
})

export class Task {
    
    task: Task;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    index: number;
    tasks = [];

    constructor(private router: Router,
                private http: Http) {

        this.title = 'Tareas';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
        //this.renderData();
    }
/*
    renderData(){
        this.taskService.getAll().subscribe((result) => {
			if ( result.status ) {
				this.tasks = result.body;
				let array = [];
			}
		})
    }

	delete(idMatrix){
        this.taskService.update(idMatrix, { deleted: 1 }).subscribe(
            result =>  {
            	if ( result ) {
                  	this.tasks.splice(this.index, 1);
              	} else {
                	this.showAlert('warning', 'fa fa-warning', 'Error al intentar borrar el registro. intente mas tarde');
              	}
        })
	}*/

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}