import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }                  from 'angular2-jwt';
import { TaskService }                 		from '../services/task.service';
import { Router, ActivatedRoute }       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;
const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-task',
    template    : template,
    styles      : [ styles ],
    providers   : [ TaskService ]
})

export class TaskEdit implements OnInit {
    
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    taskForm: FormGroup;
	idMatrix:   number;
    idReq:   	number;
	step: 		number;
	file:		boolean;
	fileUrl:	string;
	index: number;
	requeriments = [];

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private route: ActivatedRoute,private taskService: TaskService){
        window.scrollTo(0, 0);
        this.title = 'Editar matriz';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.getMatriz();
        this.step = 0;
        this.file = false;
    }
    
    ngOnInit(){
        this.taskForm = this.fb.group({
            name: '',
            name_client: '',
            name_project: '',
            version: '',
            createdAt:'',
            description: ''
        });
    }

	onEdit(){
        this.taskService.update(this.idMatrix, this.taskForm.value).subscribe(

        (result) =>  {
			this.showAlert('success', 'fa fa-check', 'Se guardo la matriz correctamente');
        },
        (error)=>{
			if(error.status === 500){
				this.showAlert('warning', 'fa fa-warning', 'Error al guardar la matriz. por favor revisar los datos.');
			} else {
				console.log("Status: "+error.status+" Message: "+error.statusText);
			}
        });
        this.onReturn();
    }

    getMatriz(){
        this.route.params.subscribe(
        params => {
            if(params['id'] !== undefined) {

                this.idMatrix = params['id'];
                this.taskService.getById(this.idMatrix).subscribe(
                	matrix => this.setMatrix(matrix.body),
                	error => console.log('Error: ' + error)
                );
            }
        });
    }

    setMatrix(matrix){
    	/*if(matrix.media_id){
    		this.file = true;
    		this.getFile(matrix.media_id);
    	}*/
    	matrix.createdAt = matrix.createdAt.slice(0, 10);
        this.taskForm = this.fb.group(matrix);
    }

    getReqs(){
        this.taskService.getReqs(this.idMatrix).subscribe(
			requeriments => this.requeriments = requeriments.body,
			error => console.log('Error: ' + error)
		);
    }

    getFile(mediaId){
        this.taskService.getFile(mediaId).subscribe(
			media => this.fileUrl = media.body[0].resource_url,
			error => console.log('Error: ' + error)
		);
		console.log(this.fileUrl);
    }

	showModal(event, idReq, i){
      	this.index = i;
      	this.idReq = idReq;
      	event.preventDefault();
      	$("#modal-deactive").modal({
        	show  : true,
        	keyboard : false
      	});
      	$(".modal-backdrop").css( "z-index", "-1" );
    }

    onReturn(){
        this.router.navigate(['/administrator/matrices'], {queryParams: {}});
    }
	
	nextStep(num){
        this.step = num;
        this.getReqs();
    }

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}