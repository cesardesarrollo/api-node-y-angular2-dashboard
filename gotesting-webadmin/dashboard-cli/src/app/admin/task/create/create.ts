import { Component, OnInit }			from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }			from 'angular2-jwt';
import { TaskService }					from '../services/task.service';
import { Task }						from '../task.model';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-task',
    template    : template,
    styles      : [ styles ],
    providers  :  [ TaskService ]
})

export class TaskCreate implements OnInit {
	task: Task;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    taskForm: FormGroup;
    idTask:   number;

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private taskService: TaskService){

    	this.task = new Task(0,'','','','',0,'','',false,0,0,0);
        this.title = 'Crear tarea';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
        this.taskForm = this.fb.group({
            title: '',
            description: '',
            start_date: '',
            end_date: '',
            cost: 0,
            state: '',
            apply: '',
            status: '',
            matriz_test_Id: 0,
            template_rin_Id: 0,
            platform_id: 0
        });
    }

    onCreate(){

    }

	created(){
  		this.redirectEdit();
    }

    redirectEdit(){
		this.router.navigate(['/administrator/matrices/edit/'+$("#created").val()], {queryParams: {}});
    }

    onReturn(){
        this.router.navigate(['/administrator/matrices'], {queryParams: {}});
    }

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}