import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { LessonService }                 from '../services/lessons.service';
import { Lesson }                        from '../lesson.model';
import { Router, ActivatedRoute }        from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-lesson',
    template    : template,
    styles      : [ styles ],
    providers   : [ LessonService ]
})

export class LessonsEdit implements OnInit{
    idLesson:   number;
    lesson: 	Lesson;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    lessonForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private route: ActivatedRoute,private lessonService: LessonService){
        window.scrollTo(0, 0);
        this.lesson = new Lesson(0,false,'','','','');
        this.title = 'Editar Lección';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt =  this.jwtHelper.decodeToken(this.jwt);
        this.getLessons();
    }

    ngOnInit(){
         this.lessonForm = this.fb.group({
            name: '',
            description: '',
            status: false 
        });
    }

    onReturn(){
        this.router.navigate(['/administrator/lessons'], {queryParams: {}});
    }

    onSubmit(){
        this.lessonService.update(this.idLesson,this.lessonForm.value).subscribe(
            result =>  {
				this.showAlert('success', 'fa fa-check', 'La lección se guardo correctamente');
            },
            (error)=>{ 
                if(error.status === 500){
					this.router.navigate(['/administrator/lessons'], {queryParams: {}});
	                this.showAlert('warning', 'fa fa-warning', 'El nombre de la Lección ya ha sido registrado, intenta con otro');
                } else {
                    console.log("Status: "+error.status+" Message: "+error.statusText);
                }
            }
        )
    }

    getLessons(){
        this.route.params.subscribe(params => {
            if(params['id'] !== undefined) {
                this.idLesson = params['id'];
                console.log(this.idLesson);
                this.lessonService.getById(this.idLesson)
                .subscribe(
                    lesson => this.setLesson(lesson.body),
                    error => console.log('Error: ' + error)
                );
            }
        });
    }

    setLesson(lesson){
        this.lessonForm = this.fb.group(lesson);
    }

    getDiagnostic(){ console.log(this.lesson) }

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}
