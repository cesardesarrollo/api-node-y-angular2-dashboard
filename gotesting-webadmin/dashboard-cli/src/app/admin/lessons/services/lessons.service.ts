import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { environment } 	  from '../../../../environments/environment';
import { Observable }     from 'rxjs/Rx';

const baseUrl = environment.base_url_api;

@Injectable()
export class LessonService {

	constructor( private http: Http){}

	create( formData ) {

		let xhr = new XMLHttpRequest();

		xhr.open('POST', baseUrl + '/lessons/new', true);
		xhr.setRequestHeader("enctype", "multipart/form-data");
		xhr.setRequestHeader("Cache-Control", "no-cache");
		xhr.setRequestHeader("token", localStorage.getItem('id_token'));
		xhr.setRequestHeader("Accept", 'application/json');
		xhr.withCredentials = true;

		xhr.onreadystatechange = function () {
            if (xhr.status === 200) {
				let result = JSON.parse(xhr.responseText);
				$("#created").click();
			} else {
				return false;
			}
		}
		xhr.send( formData );
	}
/*
	create2( data ) {
		//let body = JSON.stringify({ name, description, status });
		return this.http
			.post(
				baseUrl + '/lessons/new',
				data,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}*/

	getAll(){
		var filter = JSON.stringify({'where':{'deleted':0}});
		return this.http
			.get(
				baseUrl + '/lessons?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getById(id){
		let msg;
		return this.http
			.get(
				baseUrl + '/lessons/'+id,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	update(id,object){
		let body = JSON.stringify(object);
		let msg;
		return this.http
			.put(
				baseUrl + '/lessons/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}
	
	private handleError (error: any) {
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}

}