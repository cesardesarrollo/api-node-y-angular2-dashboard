import { Component, OnInit }				from '@angular/core';
import { Http, Headers }                	from '@angular/http';
import { AuthHttp, JwtHelper }				from 'angular2-jwt';
import { LessonService }                	from '../services/lessons.service';
import { Lesson }                       	from '../lesson.model';
import { Router }                       	from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-lesson',
    template    : template,
    styles      : [ styles ],
    providers   : [ LessonService ] 
})

export class LessonsCreate implements OnInit{
    lesson: 	Lesson;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    lessonForm: FormGroup;

    constructor(private fb: FormBuilder,private router: Router, private http: Http,private lessonService:LessonService){
        this.lesson = new Lesson(0,false,'','','','');
        this.title = 'Crear Lección';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
		this.lessonForm = this.fb.group({
            name: '',
            description: '',
            status: '',
            urlArr: this.fb.array([
                this.initVideo(),
            ]),
            file:[]
        });
    }

    initVideo(){
        return this.fb.group({
            url: ['', Validators.required],
            nameUrl: ['', Validators.required]
        });
    }

    addVideo(){
        const control = <FormArray>this.lessonForm.controls['urlArr'];
        control.push(this.initVideo());
    }

    removeVideo(i: number) {
        const control = <FormArray>this.lessonForm.controls['urlArr'];
        control.removeAt(i);
    }

    toggleVideo(i){
      	$("#video-"+i).toggleClass('actived-video');
    }

    onCreate(){
    	this.lessonForm.value.urlArr = JSON.stringify( this.lessonForm.value.urlArr );

		let formData: FormData = new FormData();
    	let xhr = new XMLHttpRequest();
		let fileList: FileList = $("#file")[0].files;

		if(fileList.length > 0) {
			for(var x = 0;x<fileList.length;x++ ){
				let file: File = fileList[x];
				formData.append('file', file, file.name);
			}
		}
		
    	formData.append('name', this.lessonForm.value.name);
    	formData.append('description', this.lessonForm.value.description);
    	formData.append('status', this.lessonForm.value.status);
    	formData.append('urlArr' ,this.lessonForm.value.urlArr);
        this.lessonService.create(formData);
/*
        this.lessonService.create( this.lessonForm.value ).subscribe(
          	(result) => {
              	this.onReturn();
        	},
	        (error)=>{ 
	            if(error.status === 500){
	                this.showAlert('warning', 'fa fa-warning', 'El nombre de la Lección ya ha sido registrado, intenta con otro');
	            } else {
	                console.log("Status: "+error.status+" Message: "+error.statusText);
	            }
	        })*/
    }

    created(){
    	this.showAlert('success', 'fa fa-check', 'El nombre de la Lección se creo correctamente');
        this.router.navigate(['/administrator/lessons'], {queryParams: {}});
    }
    onReturn(){
        this.router.navigate(['/administrator/lessons'], {queryParams: {}});
    }

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}
