import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }			from 'angular2-jwt';
import { LessonService }				from './services/lessons.service';
import { Lesson }						from './lesson.model';
import { Router }                       from '@angular/router';

declare var $:any;
const styles    = require('./lessons.css');
const template  = require('./lessons.html');

@Component({
    selector    : 'lessons',
    template    : template,
    styles      : [ styles ],
    providers   : [ LessonService ]
})

export class Lessons {
    lesson: Lesson;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    idLesson: number;
    index: number;
    lessons = [];

    constructor(private router: Router, private http: Http, private lessonService: LessonService){
        this.title = 'Lecciones';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }
    
    ngOnInit(){
       this.renderData();
    }

    renderData(){
        this.lessonService.getAll().subscribe(
        	(result) => {
				if ( result.status ) {
                  	this.lessons = result.body;
              	}
            })
    }

	onDelete(idLesson){
        this.lessonService.update(idLesson, { deleted: 1 }).subscribe(
            result =>  {
            	if ( result ) {
                  	this.lessons.splice(this.index, 1);
              	} else {
                	this.showAlert('warning', 'fa fa-warning', 'Error al intentar borrar el registro. intente mas tarde');
              	}
        })
	}

    showModal(event, idLesson, i){
      	this.index = i;
      	this.idLesson = idLesson;
      	event.preventDefault();
      	$("#modal-deactive").modal({
        	show  : true,
        	keyboard : false
      	});
      	$(".modal-backdrop").css( "z-index", "-1" );
    }

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}
