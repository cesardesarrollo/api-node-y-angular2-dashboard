import { Component, ElementRef, Renderer }  from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { LevelService }                 from './services/levels.service';
import { Level }                        from './level.model';
import { Router }                       from '@angular/router';

declare var $:any;
const styles    = require('./levels.css');
const template  = require('./levels.html');

@Component({
    selector    : 'levels',
    template    : template,
    styles      : [ styles ],
    providers   : [ LevelService ]
})

export class Levels {

    level: Level;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    idLevel: number;
    index: number;
    levels = [];

    constructor(private router: Router, private http: Http, private levelService: LevelService, private el: ElementRef, private render: Renderer){

        this.title = 'Niveles';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
        this.renderData();
    }

    renderData(){
      //this.display = new DisplayComponent;
      //console.log(this.display);
        this.levelService.getAll().subscribe((result) => {
              if ( result.status ) {
                  this.levels = result.body;
                  let array = [];
                 /* for(let i = 0;i < this.levels.length;i++){
                        array.push(
                          [
                            this.levels[i].name,
                            this.levels[i].status  == true ? 'Activo': 'Inactivo',
                            "<a [routerLink]='['edit/',"+this.levels[i].id+"]'><i class='material-icons icon-size'>mode_edit</i></a>"
                          ]); 
                  }
                  $('#data-table').DataTable({
                    data: array,
                    columns: [
                          { title: "Nombre nivel" },
                          { title: "Estatus" },
                          { title: "Acciones" }   
                    ]});*/
              } 
            })
    }
    //$('#data-table').DataTable();

    showModal(event,idLevel,i){
      this.index = i;
      this.idLevel = idLevel;
      console.log("show modal");
      event.preventDefault();
      $("#modal-deactive").modal({
        show  : true,
        keyboard : false
      });
      $(".modal-backdrop").css( "z-index", "-1" );
    }

    deactiveRecord(){
          this.levelService.getById(this.idLevel)
                 .subscribe(
                    level =>  this.updateStatus(level),
                    error => console.log('Error: ' + error)
                 );
    }

	delete(idLevel){
        this.levelService.update(idLevel, { deleted: 1 }).subscribe(
            result =>  {
            	if ( result ) {
                  	this.levels.splice(this.index, 1);
              	} else {
                	this.showAlert('warning', 'fa fa-warning', 'Error al intentar borrar el registro. intente mas tarde');
              	}
        })
	}

    updateStatus(level){
         level.body.status = false;
         console.log(level.body.status);
         console.log(level.body);
         this.levelService.update(this.idLevel,level.body)
             .subscribe(
                result =>  {  
                if ( result ) {
                    console.log(this.levels);
                     this.levels.splice(this.index, 1);
                } else {}
       })
    }

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}
