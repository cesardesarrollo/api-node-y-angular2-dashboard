import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { LevelService }                 from '../services/levels.service';
import { Level }                        from '../level.model';
import { Router, ActivatedRoute }       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-level',
    template    : template,
    styles      : [ styles ],
    providers   : [LevelService]
})

export class LevelsEdit implements OnInit {
    level: Level;
    title:      string;
    idLevel:    number;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg: string;
    levelForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private route: ActivatedRoute,private levelService: LevelService){
        window.scrollTo(0, 0);
        this.level = new Level(0,false,'','','','');
        this.title = 'Editar Nivel';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.getLevels();
    }
    
    ngOnInit(){
         this.levelForm = this.fb.group({
            name: '',
            status: '' 
        });
    }

    getLevels(){
        this.route.params.subscribe(params => {
            if(params['id'] !== undefined) {
                this.idLevel = params['id'];
                console.log(this.idLevel);
                this.levelService.getById(this.idLevel)
                 .subscribe(
                    level => this.setLevel(level.body),
                    error => console.log('Error: ' + error)
                 );
            }
        });
    }
    setLevel(level){
        console.log(this.levelForm);
        this.levelForm = this.fb.group(level);
        console.log(this.levelForm);
    }

    onReturn(){
        this.router.navigate(['/administrator/levels'], {queryParams: {}});
    }

    onSubmit(){
        this.levelService.update(this.idLevel,this.levelForm.value)
             .subscribe(
                result =>  {               
                    this.msg = 'Se editó el nivel';
                    $('#alert-success').show();
                    setTimeout(function() {
                        $('#alert-success').hide();       
                    }, 2000);
            },
             (error)=>{ 
                if(error.status === 500){
                    this.msg = 'El nombre del nivel ya ha sido registrado, intenta con otro.';
                    $('#alert-danger').show();
                    setTimeout(function() {
                        $('#alert-danger').hide();       
                    }, 2000);
                } else {
                    console.log("Status: "+error.status+" Message: "+error.statusText);
                }
            });
    }

}