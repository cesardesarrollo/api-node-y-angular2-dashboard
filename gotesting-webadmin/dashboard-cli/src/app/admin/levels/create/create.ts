import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { LevelService }                 from '../services/levels.service';
import { Level }                        from '../level.model';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-level',
    template    : template,
    styles      : [ styles ],
    providers  :  [ LevelService ]
})

export class LevelsCreate implements OnInit {
    level: Level;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    levelForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http,private levelService: LevelService){
       
        this.level = new Level(0,null,'','','','');
        this.title = 'Crear Nivel';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
         this.levelForm = this.fb.group({
            name: '',
            status: '' 
        });
    }

    levelCreate(){

        this.levelService.create( this.levelForm.value.name , this.levelForm.value.status ).subscribe((result) => {
            //console.log(result);
            this.onReturn();    
        },
        (error)=>{ 
            if(error.status === 500){
                this.msg = 'El nombre de nivel ya ha sido registrado, intenta con otro.';
                $('#alert-danger').show();
                setTimeout(function() {
                     $('#alert-danger').hide();       
                }, 2000);
            }else{
                console.log("Status: "+error.status+" Message: "+error.statusText);
            }
        })
    }

   onReturn(){
        this.router.navigate(['/administrator/levels'], {queryParams: {}});
    }
}