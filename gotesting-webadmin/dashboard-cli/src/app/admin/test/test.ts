import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./test.css');
const template  = require('./test.html');

@Component({
    selector    : 'test',
    template    : template,
    styles      : [ styles ]
})

export class Test {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router, private http: Http){
        this.title = 'Test view';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    logout(){
        localStorage.removeItem('id_token');
        this.router.navigate(['/login']);
    }
}