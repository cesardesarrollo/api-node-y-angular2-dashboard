import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../common/headers';
import { enviroment } 	  from '../../common/enviroments.ts';

const baseUrl = enviroment.base_url_api;

@Injectable()
export class AgreementService {

	constructor( private http: Http){}

	getAgreements(){
        return this.http
        .get(
            baseUrl + '/agreements',
            { headers : contentHeaders }
        )
        .map( (res) => {
        	console.log(res);
            if( res.status === 200 ) {
                return {status: true, body: res.json() };
            } else {
                return {status: false, body: {}};
            }
        });
	}
}