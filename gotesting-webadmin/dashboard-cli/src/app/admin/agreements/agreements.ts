import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';
import { AgreementService }             from './agreements.service';
import * as moment from 'moment'

const styles    = require('./agreements.css');
const template  = require('./agreements.html');

@Component({
    selector    : 'agreements',
    template    : template,
    styles      : [ styles ]
})

export class Agreements {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    agreements = [];

    constructor(private router: Router, private http: Http, private as: AgreementService){
        this.title = 'Acuerdos';
        this.jwt = localStorage.getItem('id_token');
        //this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
        this.getAgreements();
    }

    getAgreements(){
        //console.log(moment);
        this.as.getAgreements().subscribe(
            res => this.setAgreetments(res.body),
            error => console.log(error) 
        )
    }

    setAgreetments(agreements){
        console.log(agreements);
        this.agreements = agreements;

        for(let t in agreements){
            console.log(agreements[t].updatedAt);
           let agreetmentUpdate = moment.parseZone(agreements[t].updatedAt).utc().format();
           console.log(agreetmentUpdate);
           //let currentMoment = moment.parseZone(moment()).utc().format();
           //console.log(currentMoment);
           //let currentMomentUTC = moment.parseZone(currentMoment).utc().format();
           //console.log(currentMomentUTC);
           //let currentMomentMs  = moment( new Date(currentMomentUTC).getTime() );
           //console.log(currentMomentMs);
           console.log(new Date( agreetmentUpdate ).getTime());
           //console.log(moment.parseZone(new Date( agreetmentUpdate ).getTime() + (10*60*60*1000)).utc().format());
        }

    }

}