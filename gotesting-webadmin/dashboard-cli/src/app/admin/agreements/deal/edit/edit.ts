import { Component,OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { DealService }                 from '../services/deal.service';
import { Deal }                         from '../deal.model';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-deal',
    template    : template,
    styles      : [ styles ],
    providers  : [ DealService ]
})

export class DealEdit implements OnInit {
    deal:         Deal;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    editable: boolean;
    msg: string;
    dealForm: FormGroup;

    constructor(private fb: FormBuilder,private router: Router, private http: Http, private dealService: DealService){
        window.scrollTo(0, 0);
        this.deal = new Deal(0,false,'','','','','');
        this.editable = false;
        this.title = 'Texto convenio';
        this.jwt = localStorage.getItem('id_token');
       // this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    ngOnInit(){ 
        this.dealForm = this.fb.group({
            description: ''
        })
        if($('#menu-trigger').attr('class') != ''){
             $('#menu-trigger').toggleClass('open');
             $('#sidebar').toggleClass('toggled');
        }    
        this.getDeal();
    }

    getDeal(){
       this.dealService.getDeal().subscribe( (result) => {
             this.dealForm = this.fb.group(result.body[0]);
             //console.log(result.body[0]);
       });
    }

    saveText(){
       //console.log($("#description").summernote('code'));
       this.dealForm.value.description = $("#description").summernote('code');
       this.dealService.updateDeal(this.dealForm.value.id,this.dealForm.value).subscribe( (result) => {
           if(result){
               this.onReturn();
           }
       });
    }

    onReturn(){
         this.router.navigate(['/administrator/levels'], {queryParams: {}});
    }

    editText(){
        $("#description").summernote({
            toolbar: [
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['style', ['bold', 'italic', 'underline', 'clear']]
          ]}
        );
        $("#description").prop('disabled', false);
    }

}