export class Term {
  constructor(
  	public id: number,
    public status: boolean,
    public type: string,
    public description: string,
    public title: string,
    public createdAt: string,
    public updatedAt: string
  ) {  }
}
/*
{
  "type": "",
  "description": "",
  "status": false,
  "createdAt": "2016-09-07T00:00:00.000Z",
  "updatedAt": "2016-09-07T17:01:32.000Z",
  "id": 1
}
*/