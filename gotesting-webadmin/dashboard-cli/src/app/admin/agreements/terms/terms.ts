import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { TermsService }                 from './services/terms.service';
import { Term }                         from './term.model';
import { Router }                       from '@angular/router';

declare var $:any;

const styles    = require('./terms.css');
const template  = require('./terms.html');

@Component({
    selector    : 'terms',
    template    : template,
    styles      : [ styles ],
    providers   : [ TermsService ]
})

export class Terms {
    term:         Term;
    idTerm:     number;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;

    constructor(private router: Router, private http: Http, private termsService: TermsService){
        this.idTerm = 1;
        this.term = new Term(0,false,'','','','','');
        this.title = 'Términos y condiciones';
        this.jwt = localStorage.getItem('id_token');
        //this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
        this.renderData();     
    }

    ngOnInit(){ 
        if($('#menu-trigger').attr('class') != ''){
             $('#menu-trigger').toggleClass('open');
             $('#sidebar').toggleClass('toggled');
        }    
    }

    renderData(){
        this.term

        
        this.termsService.getById(this.idTerm)
            .subscribe(
                term => this.term = term.body,
                error => console.log('Error: ' + error)
            );
    }

}