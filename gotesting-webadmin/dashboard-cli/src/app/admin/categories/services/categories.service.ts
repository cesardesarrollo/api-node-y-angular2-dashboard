import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { environment } 	  from '../../../../environments/environment';

const baseUrl = environment.base_url_api;

@Injectable()
export class CategoryService {

	constructor( private http: Http){}

	create( category ) {
		let body = JSON.stringify( category );
		console.log(body);
		let msg;
		return this.http
			.post(
				baseUrl + '/categories',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			});
	}

	update(id, object){
		let body = JSON.stringify( object );
		let msg;
		return this.http
			.put(
				baseUrl + '/categories/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			});
	}

	getById(id){
		let msg;
		return this.http
			.get(
				baseUrl + '/categories/'+id,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}


	getAll(){
		var filter = JSON.stringify({'where':{'status':'true'}});
		let msg;
		return this.http
			.get(
				baseUrl +'/categories?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}


}