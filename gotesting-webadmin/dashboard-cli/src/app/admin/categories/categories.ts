import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { CategoryService }              from './services/categories.service';
import { Category }                     from './categories.model';
import { Router }                       from '@angular/router';

declare var $:any;

const styles    = require('./categories.css');
const template  = require('./categories.html');

@Component({
    selector    : 'categories',
    template    : template,
    styles      : [ styles ],
    providers   : [ CategoryService ]  
})

export class Categories {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    index: number;
    category: Category;
    idCategory: number;
    categories = [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router, private http: Http, private categoryService: CategoryService){
        this.title = 'Catálogo Categorías';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }
    
    ngOnInit(){
        if($('#menu-trigger').attr('class') != ''){
            $('#menu-trigger').toggleClass('open');
            $('#sidebar').toggleClass('toggled');
        }   
        this.renderData();
    }

    renderData(){
        this.categoryService.getAll().subscribe((result) => {
            if ( result.status ) {
                this.categories = result.body;
            } else {
                //this.msg = 'Ocurrio un error al registrar';
                //$('#alert-danger').show();
            }
        })
    }

    showModal(event,idCategory,i){
      this.index = i;
      this.idCategory = idCategory;
      event.preventDefault();
      $("#modal-deactive").modal({
        show  : true,
        keyboard : false
      });
      $(".modal-backdrop").css( "z-index", "-1" );
    }

    deactiveRecord(){
          this.categoryService.getById(this.idCategory)
                 .subscribe(
                    category =>  this.updateStatus(category),
                    error => console.log('Error: ' + error)
                 );   
    }

    updateStatus(category){
        category.body.status = false;
        this.categoryService.update(this.idCategory,category.body)
          .subscribe(
              result =>  {  
              if ( result ) {
                  console.log(this.categories);
                  this.categories.splice(this.index, 1);
              } else {
                 //
              }
          })
        }
    }
