import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { CategoryService }                 from '../services/categories.service';
import { Category }                     from '../categories.model';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-category',
    template    : template,
    styles      : [ styles ],
    providers   : [ CategoryService ]
})

export class CategoriesCreate implements OnInit {
    category: Category;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    categoryForm: FormGroup;

    constructor(private fb:FormBuilder, private router: Router, private http: Http, private categoryService: CategoryService){
        this.category = new Category(0,false,'');
        this.title = 'Agregar Categoría';
        this.jwt = localStorage.getItem('id_token');
        //this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
         this.categoryForm = this.fb.group({
            name: '',
            status: '' 
        });
    }

    categoryCreate(){         
      this.categoryService.create( this.categoryForm.value ).subscribe((result) => {
        if ( result ) {
            this.onReturn();
        } else {
            this.msg = 'Ocurrio un error al registrar';
            $('#alert-danger').show();
            setTimeout(function() {
                 $('#alert-danger').hide();       
            }, 2000);
        }
      })
    }

    onReturn(){
      this.router.navigate(['/administrator/categories'], {queryParams: {}});
    }
}
