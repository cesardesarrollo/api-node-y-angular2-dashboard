import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { Category }                     from '../categories.model';
import { CategoryService }              from '../services/categories.service';
import { Router,ActivatedRoute }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-category',
    template    : template,
    styles      : [ styles ],
    providers   : [ CategoryService ]
})

export class CategoriesEdit {
    category: Category;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    idCategory:   number;
    msg:        string;
    categoryForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();
    
    constructor(private fb:FormBuilder, private router: Router, private http: Http, private route: ActivatedRoute,private categoryService: CategoryService){
        window.scrollTo(0, 0);
        this.category = new Category(0,false,'');
        this.title = 'Editar Categoría';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.getCategory();
    }

    ngOnInit(){
         this.categoryForm = this.fb.group({
            name: '',
            status: false 
        });
    }

    getCategory(){
        this.route.params.subscribe(params => {
            if(params['id'] !== undefined) {
                this.idCategory = params['id'];
                console.log(this.idCategory);
                this.categoryService.getById(this.idCategory)
                 .subscribe(
                    category => this.setCategory(category.body),
                    error => console.log('Error: ' + error)
                 );
            }
        });
    }

    setCategory(category){
        this.categoryForm = this.fb.group(category);
    }    

    onReturn(){
        this.router.navigate(['/administrator/categories'], {queryParams: {}});
    }

    onSubmit(){
        this.categoryService.update(this.idCategory,this.categoryForm.value)
                 .subscribe(
                    result =>  {  
                    if ( result ) {
                        this.msg = 'Se editó la categoría';
                        $('#alert-success').show();
                        setTimeout(function() {
                            $('#alert-success').hide();       
                        }, 2000);
                    } else {
                        this.msg = 'Ocurrio un error al editar';
                        $('#alert-danger').show();
                        setTimeout(function() {
                             $('#alert-danger').hide();       
                        }, 2000);
                    }
                });
    }
}
