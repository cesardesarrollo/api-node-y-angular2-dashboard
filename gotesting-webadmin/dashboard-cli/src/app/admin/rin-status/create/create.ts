import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }					from 'angular2-jwt';
import { RinService }              				from '../services/rin-status.service';
import { RinStatusModel }						from '../rin-status.model';
import { Router }                       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-rin-status',
    template    : template,
    styles      : [ styles ],
    providers   : [ RinService ]
})

export class RinStatusCreate implements OnInit {
    rinStatusModel: RinStatusModel;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    rinStatusForm: FormGroup;
    platforms = [];
    producers = [];
    categories = [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private rinService: RinService){
        this.rinStatusModel = new RinStatusModel(0, '', '');
        this.title = 'Agregar estatus';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
        this.rinStatusForm = this.fb.group({
            name: '',
            id: '',
            status: ''
        });
    }

    rinStatusCreate(){
      	this.rinService.create( this.rinStatusForm.value ).subscribe((result) => {
        	if ( result ) {
            	this.onReturn();
        	} else {
            	this.msg = 'Ocurrio un error al registrar';
            	$('#alert-danger').show();
            	setTimeout(function() {
                 	$('#alert-danger').hide();       
            	}, 2000);
        	}
      	})
    }

    onReturn(){
      	this.router.navigate(['/administrator/rin-status'], {queryParams: {}});
    }
}
