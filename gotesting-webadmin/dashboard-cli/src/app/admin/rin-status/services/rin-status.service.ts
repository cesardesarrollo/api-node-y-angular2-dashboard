import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { environment } 	  from '../../../../environments/environment';

const baseUrl = environment.base_url_api;

@Injectable()
export class RinService {

	constructor( private http: Http){}

	create( platform ) {
		platform.administrator_Id = 1;
		let body = JSON.stringify( platform );
		let msg;
		return this.http
			.post(
				baseUrl + '/devices',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			});
	}

	update(id, object){
		let body = JSON.stringify( object );
		let msg;
		return this.http
			.put(
				baseUrl + '/devices/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			});
	}

	getById(id){
		let msg;
		return this.http
			.get(
				baseUrl + '/devices/'+id,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}


	getAll(){
		var filter = JSON.stringify({'where':{'deleted':0}});
		let msg;
		return this.http
			.get(
				baseUrl +'/devices?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getPlatforms(){
		return this.http
			.get(
				baseUrl + '/platforms',
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getCategories(){
		return this.http
			.get(
				baseUrl + '/categories',
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getProducers(){
		return this.http
			.get(
				baseUrl + '/producers',
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

}