import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }			from 'angular2-jwt';
import { RinService }              	from './services/rin-status.service';
import { Router }                       from '@angular/router';

declare var $:any;

const styles    = require('./rin-status.css');
const template  = require('./rin-status.html');

@Component({
    selector    : 'rin-status',
    template    : template,
    styles      : [ styles ],
    providers   : [ RinService ]  
})

export class RinStatus {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    index: number;
    idRin: number;
    rins = [];
    msg = '';

    constructor(private router: Router, private http: Http, private rinService: RinService){
        this.title = 'Catalogo status';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt =  this.jwtHelper.decodeToken(this.jwt);
    }
    
    ngOnInit(){
        this.getData();
    }

    getData(){
        this.rinService.getAll().subscribe((result) => {
            if ( result.status ) {
                this.rins = result.body;
            } else {
                this.msg = 'No se encontraron datos';
            }
        })
    }

	showModal(event, idRin, i){
		
      	this.index = i;
      	this.idRin = idRin;
      	event.preventDefault();

      	$("#modal-deactive").modal({
        	show  : true,
        	keyboard : false
      	});
      	$(".modal-backdrop").css( "z-index", "-1" );
    }

    delete(idRin){
        this.rinService.update(idRin, { deleted: 1 })
        .subscribe(
            result =>  {
            	if ( result ) {
                  	this.rins.splice(this.index, 1);
              	} else {
                	this.msg = "Error al intentar borrar el registro. intente mas tarde";
                	$('#alert-danger').show();
              	}
        })
	}
}