import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }					from 'angular2-jwt';
import { PricingService }              				from '../services/pricing.service';
import { PricingModel }						from '../pricing.model';
import { Router }                       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-pricing',
    template    : template,
    styles      : [ styles ],
    providers   : [ PricingService ]
})

export class PricingCreate implements OnInit {
    pricingModel: PricingModel;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    pricingForm: FormGroup;
    platforms = [];
    producers = [];
    categories = [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private pricingService: PricingService){
        this.pricingModel = new PricingModel(0, '', '');
        this.title = 'Agregar plan';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
        this.pricingForm = this.fb.group({
            name: '',
            id: '',
            status: ''
        });
    }

    pricingCreate(){
      	this.pricingService.create( this.pricingForm.value ).subscribe((result) => {
        	if ( result ) {
            	this.onReturn();
        	} else {
            	this.msg = 'Ocurrio un error al registrar';
            	$('#alert-danger').show();
            	setTimeout(function() {
                 	$('#alert-danger').hide();       
            	}, 2000);
        	}
      	})
    }

    onReturn(){
      	this.router.navigate(['/administrator/pricing'], {queryParams: {}});
    }
}
