import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }			from 'angular2-jwt';
import { PricingService }              		from './services/pricing.service';
import { Router }                       from '@angular/router';

declare var $:any;

const styles    = require('./pricing.css');
const template  = require('./pricing.html');

@Component({
    selector    : 'pricing',
    template    : template,
    styles      : [ styles ],
    providers   : [ PricingService ]  
})

export class Pricing {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    index: number;
    idPricing: number;
    pricing = [];
    msg = '';

    constructor(private router: Router, private http: Http, private pricingService: PricingService){
        this.title = 'Planes y costos';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt =  this.jwtHelper.decodeToken(this.jwt);
    }
    
    ngOnInit(){
        this.getData();
    }

    getData(){
        this.pricingService.getAll().subscribe((result) => {
            if ( result.status ) {
                this.pricing = result.body;
            } else {
                this.msg = 'No se encontraron datos';
            }
        })
    }

	showModal(event, idPricing, i){
		
      	this.index = i;
      	this.idPricing = idPricing;
      	event.preventDefault();

      	$("#modal-deactive").modal({
        	show  : true,
        	keyboard : false
      	});
      	$(".modal-backdrop").css( "z-index", "-1" );
    }

    delete(idPricing){
        this.pricingService.update(idPricing, { deleted: 1 })
        .subscribe(
            result =>  {
            	if ( result ) {
                  	this.pricing.splice(this.index, 1);
              	} else {
                	this.msg = "Error al intentar borrar el registro. intente mas tarde";
                	$('#alert-danger').show();
              	}
        })
	}
}