import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }					from 'angular2-jwt';
import { RinService }              				from '../services/rin-severity.service';
import { RinSeverityModel }						from '../rin-severity.model';
import { Router }                       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-rin-severity',
    template    : template,
    styles      : [ styles ],
    providers   : [ RinService ]
})

export class RinSeverityCreate implements OnInit {
    rinSeverityModel: RinSeverityModel;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    rinSeverityForm: FormGroup;
    platforms = [];
    producers = [];
    categories = [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private rinService: RinService){
        this.rinSeverityModel = new RinSeverityModel(0, '', '');
        this.title = 'Agregar severidad';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
        this.rinSeverityForm = this.fb.group({
            name: '',
            id: '',
            status: ''
        });
    }

    rinSeverityCreate(){
      	this.rinService.create( this.rinSeverityForm.value ).subscribe((result) => {
        	if ( result ) {
            	this.onReturn();
        	} else {
            	this.msg = 'Ocurrio un error al registrar';
            	$('#alert-danger').show();
            	setTimeout(function() {
                 	$('#alert-danger').hide();       
            	}, 2000);
        	}
      	})
    }

    onReturn(){
      	this.router.navigate(['/administrator/rin-severity'], {queryParams: {}});
    }
}
