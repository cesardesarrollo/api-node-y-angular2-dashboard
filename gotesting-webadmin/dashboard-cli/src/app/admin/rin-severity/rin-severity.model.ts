export class RinSeverityModel {
  constructor(
  	public id: number,
    public name: string,
    public status: string
  ) {  }
}