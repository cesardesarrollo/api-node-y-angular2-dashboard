import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';
import { CourseService }                from './services/courses.service';

declare var $:any;
const styles    = require('./courses.css');
const template  = require('./courses.html');

@Component({
    selector    : 'courses',
    template    : template,
    styles      : [ styles ],
    providers : [ CourseService ]
})

export class Courses {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    courses = [];
	index: number;
    idCourse: number;


    constructor(private router: Router, private http: Http, private courseService: CourseService){
        this.title = 'Catálogo Cursos';
        this.jwt = localStorage.getItem('id_token');
        //this.decodedJwt = this.jwt && window.jwt_decode(this.jwt);
    }

    ngOnInit(){
        this.getData();
    }

    getData(){
        this.courseService.getAll().subscribe(
        	(result) => {
	            if ( result.status ) {
	                this.courses = result.body;
	            }
	        })
    }

	onDelete( idCourse ){
        this.courseService.update(idCourse, { deleted: 1 })
        .subscribe(
            result =>  {
            	if ( result ) {
                  	this.courses.splice(this.index, 1);
              	} else {
                	this.showAlert('warning', 'fa fa-warning', 'Error al intentar borrar el registro. intente mas tarde');
              	}
        })
	}

	showModal(event, idCourse, i){
      	this.index = i;
      	this.idCourse = idCourse;
      	event.preventDefault();

      	$("#modal-deactive").modal({
        	show  : true,
        	keyboard : false
      	});
      	$(".modal-backdrop").css( "z-index", "-1" );
    }

	showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}
