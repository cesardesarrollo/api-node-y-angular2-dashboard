import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { environment } 	  from '../../../../environments/environment';
import { Observable }     from 'rxjs/Rx';
import { AuthService }    from '../../../common/auth.service';

const baseUrl = environment.base_url_api;
let headers = contentHeaders;

@Injectable()
export class CourseService {

	constructor( private http: Http, private as: AuthService){}

	create( data ) {
		headers.append('token', this.as.getUserToken());
		
		return this.http
			.post(
				baseUrl + '/courses/new',
				data,
				{ headers : headers }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}

	getAll(){
		var filter = JSON.stringify( {"where":{"deleted":0}, "include":["dev_platform_Id", "levels_Id"] } );
		return this.http
			.get(
				baseUrl + '/courses?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getPlatforms(){
		var filter = JSON.stringify({'where':{'status':'true'}});
		return this.http
			.get(
				baseUrl + '/platforms?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getLevels(){
		var filter = JSON.stringify({'where':{'status':'true'}});
		return this.http
			.get(
				baseUrl + '/levels?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getLessons(){
		var filter = JSON.stringify({'where':{'status':'true'}});
		return this.http
			.get(
				baseUrl + '/lessons?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	createLesson( data ) {
		data.urlArr = ' ';
		return this.http
			.post(
				baseUrl + '/lessons/new',
				data,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}

	getById(id){
		return this.http
			.get(
				baseUrl + '/courses/'+id,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	update(id, data){
		delete data.levels_id;
		delete data.platform_id;
		delete data.end_date;
		delete data.start_date;
		console.log(data);
		return this.http
			.put(
				baseUrl + '/courses/'+id,
				data,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res);
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}
	
	private handleError (error: any) {
		//console.log(error);
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}
}