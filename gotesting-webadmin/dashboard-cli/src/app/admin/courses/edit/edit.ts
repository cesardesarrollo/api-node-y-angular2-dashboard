import { Component }                    	from '@angular/core';
import { Http, Headers }                	from '@angular/http';
import { AuthHttp, JwtHelper }				from 'angular2-jwt';
import { Router, ActivatedRoute }                       	from '@angular/router';
import { CourseService }                	from '../services/courses.service';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

declare var $:any;
const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-courses',
    template    : template,
    styles      : [ styles ],
    providers   : [ CourseService ]
})

export class CoursesEdit {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
	jwtHelper: JwtHelper = new JwtHelper();
    courseForm: FormGroup;
    lessonForm: FormGroup;
    autocompleteForm:FormGroup;
    idCourse: number;
    platforms = [];
    levels = [];
    lessons = [];
    price:      number; 
    filteredLessons: any[];
    autoc: any[];
    image: string;

    constructor(private router: Router, private http: Http,private fb: FormBuilder, private courseService: CourseService, private route: ActivatedRoute){
        this.title = 'Editar Curso';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.courseForm = this.fb.group({
            name: '',
            description: '',
            start_date: '',
            end_date: '',
            platform_id: '',
            version: '',
            price: 0,
            status: '',
            levels_id:'',
            lessons_arr: []
        });
        this.getCourse();

        this.lessonForm = this.fb.group({
            name: '',
            status: '',
            description: ''
        });
        this.autocompleteForm = this.fb.group({
            autocomplete: ''
        });
    }

    ngOnInit(){
        this.Platforms.subscribe( (res) =>{ this.Platforms = res.body; },
                                  (err) =>{ console.log(err); } );
        this.Levels.subscribe((res) => { this.Levels = res.body;}, 
                                  (err) =>{ console.log(err);} );
        this.Lessons.subscribe((res) => { this.lessons = res.body }, 
                                  (err) =>{ console.log(err);} );
    }

	getCourse(){
        this.route.params.subscribe(params => {
            if(params['id'] !== undefined) {
                this.idCourse = params['id'];
                this.courseService.getById(this.idCourse)
                .subscribe(
                   	course => this.setDevice(course.body),
                    error => console.log('Error: ' + error)
                );
            }
        });
    }

    setDevice(course){
        course.response.course.start_date = this.formatDate(course.response.course.startDate);
        course.response.course.end_date = this.formatDate(course.response.course.endDate);
        course.response.course.platform_id = course.response.course.dev_platform_Id;
        course.response.course.levels_id = course.response.course.levels_Id;
        course.response.course.lessons_arr = [];
        this.image = course.response.course.cover;
        this.price = course.response.course.price;
        if(this.price){
        	$("#notfree").prop('checked', true);
        }
        this.courseForm = this.fb.group(course.response.course);

    	let lessonsArr = [];
        for(let i = 0; i < course.response.lessons.length; i++) {
            lessonsArr.push(course.response.lessons[i].lesson_data);
        }
        this.autoc = lessonsArr;
    }

	onUpdate(){
    	let lessonsArr = [];
    	for(let i = 0; i < this.autoc.length; i++) {
            lessonsArr.push(this.autoc[i].id);
        }
        this.courseForm.value.lessons_arr = JSON.stringify(lessonsArr);
    	this.courseForm.value.cover = this.image;
    	this.courseForm.value.price = this.price;

    	this.courseService.update( this.idCourse, this.courseForm.value ).subscribe(
    		(result) => {
		        if ( result ) {
					this.showAlert('success', 'fa fa-check', 'Curso modificado correctamente');
		            this.onReturn();
		        } else {
					this.showAlert('warning', 'fa fa-warning', 'Error al intentar guardar el registro. intente mas tarde');
		        }
      		})
    }

    autocompleteLesson(event) {
        this.filteredLessons = [];
        for(let i = 0; i < this.lessons.length; i++) {
            let lesson = this.lessons[i];
            if(lesson.name.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
                this.filteredLessons.push(lesson);
            }
        }
    }

    onLoadImage($event) : void {
	  	this.toBase64($event.target);
	}

	toBase64(inputValue: any): void {
	  	var file:File = inputValue.files[0];
	  	var myReader:FileReader = new FileReader();

	  	myReader.onloadend = (e) => {
	    	this.image = myReader.result;
	  	}
	  	myReader.readAsDataURL(file);
	}

	onCreateLesson(){
    	this.courseService.createLesson( this.lessonForm.value ).subscribe(
    		(result) => {
		        if ( result ) {
					this.showAlert('success', 'fa fa-check', 'Leccion creada correctamente');
		        }
      		})
    }

    openModal(){
        $("#modal-lessons").modal({
            show  : true,
            keyboard : false
        });
        $(".modal-backdrop").css( "z-index", "-1" );
         console.log(this.levels);
         console.log(this.platforms);
    }

    onReturn(){
          this.router.navigate(['/administrator/courses'], {queryParams: {}});
    }

    formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}

    set Levels(levels: any){ this.levels = levels; }

    get Levels(): any{ return this.courseService.getLevels(); }

    set Platforms(platforms: any){ this.platforms = platforms; }

    get Platforms(): any{ return this.courseService.getPlatforms(); }

    set Lessons(lessons: any){ this.lessons = lessons; }

    get Lessons(): any{ return this.courseService.getLessons(); }

	showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}
