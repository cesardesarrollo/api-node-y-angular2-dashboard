import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }					from 'angular2-jwt';
import { RinService }              				from '../services/rin-error.service';
import { RinErrorModel }						from '../rin-error.model';
import { Router }                       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-rin-error',
    template    : template,
    styles      : [ styles ],
    providers   : [ RinService ]
})

export class RinErrorCreate implements OnInit {
    rinErrorModel: RinErrorModel;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    rinErrorForm: FormGroup;
    platforms = [];
    producers = [];
    categories = [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private rinService: RinService){
        this.rinErrorModel = new RinErrorModel(0, '', '');
        this.title = 'Agregar severidad';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
        this.rinErrorForm = this.fb.group({
            name: '',
            id: '',
            status: ''
        });
    }

    rinErrorCreate(){
      	this.rinService.create( this.rinErrorForm.value ).subscribe((result) => {
        	if ( result ) {
            	this.onReturn();
        	} else {
            	this.msg = 'Ocurrio un error al registrar';
            	$('#alert-danger').show();
            	setTimeout(function() {
                 	$('#alert-danger').hide();       
            	}, 2000);
        	}
      	})
    }

    onReturn(){
      	this.router.navigate(['/administrator/rin-error'], {queryParams: {}});
    }
}
