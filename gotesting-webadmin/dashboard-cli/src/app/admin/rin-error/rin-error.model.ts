export class RinErrorModel {
  constructor(
  	public id: number,
    public name: string,
    public status: string
  ) {  }
}