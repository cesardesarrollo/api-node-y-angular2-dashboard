import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }					from 'angular2-jwt';
import { RinService }              			from '../services/rins.service';
import { Rin }                     			from '../rins.model';
import { Router }                       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } 	from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-rin',
    template    : template,
    styles      : [ styles ],
    providers   : [ RinService ]
})

export class RinsCreate implements OnInit {
    rin: Rin;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    rinForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();
    severities: any[];
    defects: any[];
    states: any[];

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private rinService: RinService){
        this.rin = new Rin(0, '', '', '', '', '', '', '', '');
        this.title = 'Agregar plantilla RIN';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
        this.severities = [
        	{name:'Bloqueadora', check:true}, 
        	{name:'Crítica', check:true},
        	{name:'Menor', check:true},
        	{name:'Trivial', check:true}
        ];
        this.defects = [
        	{name:'Performance', check:true}, 
        	{name:'Cosméticos', check:true},
        	{name:'Error ortográfico', check:true},
        	{name:'Error de excepción', check:true},
        	{name:'Interfaz de usuario', check:true},
        	{name:'Error de cálculo', check:true},
        	{name:'Control de flujo', check:true}
        ];
        this.states = [
        	{name:'Reportado', check:true}, 
        	{name:'Regresado', check:true},
        	{name:'Corregido', check:true}
        ];
    }

    ngOnInit(){
        this.rinForm = this.fb.group({
            name: '',
            severity:'',
            type_defect:'',
            state:''
        });
    }

    addSeverity(){
    	this.severities.push({name:this.rinForm.value.severity, check:true});
    }

    checkedSeverity(i){
    	if(this.severities[i]){
    		this.severities[i].check = false;
    	}else{
    		this.severities[i].check = true;
    	}
    }

    addDefect(){
    	this.defects.push({name:this.rinForm.value.type_defect, check:true});
    }

    checkedDefect(i){
    	if(this.defects[i]){
    		this.defects[i].check = false;
    	}else{
    		this.defects[i].check = true;
    	}
    }

    addState(){
    	this.states.push({name:this.rinForm.value.state, check:true});
    }

    checkedState(i){
    	if(this.states[i]){
    		this.states[i].check = false;
    	}else{
    		this.states[i].check = true;
    	}
    }

    onCreate(){
    	this.rinForm.value.severity = JSON.stringify(this.severities);
    	this.rinForm.value.type_defect = JSON.stringify(this.defects);
    	this.rinForm.value.state = JSON.stringify(this.states);
    	
    	console.log(this.rinForm.value);
      	this.rinService.create( this.rinForm.value ).subscribe(
      		(result) => {
		        if ( result ) {
		            this.onReturn();
		        } else {
		            this.msg = 'Ocurrio un error al registrar';
		            $('#alert-danger').show();
		            setTimeout(function() {
		                 $('#alert-danger').hide();       
		            }, 2000);
		        }
			})
    }

    onReturn(){
      	this.router.navigate(['/administrator/rins'], {queryParams: {}});
    }
}
