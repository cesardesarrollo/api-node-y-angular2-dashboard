export class Rin {
  constructor(
  	public id: number,
    public name: string,
    public module: string,
    public status: string,
    public version: string,
    public comments: string,
    public title: string,
    public type: string,
    public severity: string
  ) {  }
}