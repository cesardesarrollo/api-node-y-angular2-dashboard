import { Component }         from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { ProducerService }              from './services/producers.service';
import { Producer }                     from './producers.model';
import { Router }                       from '@angular/router';

declare var $:any;

const styles    = require('./producers.css');
const template  = require('./producers.html');

@Component({
    selector    : 'producers',
    template    : template,
    styles      : [ styles ],
    providers   : [ ProducerService ]  
})

export class Producers {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    index: number;
    producer: Producer;
    idProducer: number;
    producers = [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router, private http: Http, private producerService: ProducerService){
        this.title = 'Catálogo Fabricantes';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }
    
    ngOnInit(){
        if($('#menu-trigger').attr('class') != ''){
            $('#menu-trigger').toggleClass('open');
            $('#sidebar').toggleClass('toggled');
        }   
        this.renderData();
    }

    renderData(){
        this.producerService.getAll().subscribe((result) => {
            if ( result.status ) {
                this.producers = result.body;
            } else {
            }
        })
    }

    showModal(event,idProducer,i){
      this.index = i;
      this.idProducer = idProducer;
      event.preventDefault();
      $("#modal-deactive").modal({
        show  : true,
        keyboard : false
      });
      $(".modal-backdrop").css( "z-index", "-1" );
    }

    deactiveRecord(){
          this.producerService.getById(this.idProducer)
                 .subscribe(
                    producer =>  this.updateStatus(producer),
                    error => console.log('Error: ' + error)
                 );   
    }

    updateStatus(producer){
        producer.body.status = false;
        this.producerService.update(this.idProducer,producer.body)
          .subscribe(
              result =>  {  
              if ( result ) {
                  console.log(this.producers);
                  this.producers.splice(this.index, 1);
              } else {
                 //
              }
          })
        }
    }
