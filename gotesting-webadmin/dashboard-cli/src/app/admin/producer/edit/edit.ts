import { Component }         from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }          from 'angular2-jwt';
import { Producer }                     from '../producers.model';
import { ProducerService }              from '../services/producers.service';
import { Router,ActivatedRoute }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-producer',
    template    : template,
    styles      : [ styles ],
    providers   : [ ProducerService ]
})

export class ProducersEdit {
    producer: Producer;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    idProducer:   number;
    msg:        string;
    producerForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();
    
    constructor(private fb:FormBuilder, private router: Router, private http: Http, private route: ActivatedRoute,private producerService: ProducerService){
        window.scrollTo(0, 0);
        this.producer = new Producer(0,'',false);
        this.title = 'Editar Fabricante';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.getProducers();
    }

    ngOnInit(){
         this.producerForm = this.fb.group({
            name: '',
            status: false 
        });
    }

    getProducers(){
        this.route.params.subscribe(params => {
            if(params['id'] !== undefined) {
                this.idProducer = params['id'];
                console.log(this.idProducer);
                this.producerService.getById(this.idProducer)
                 .subscribe(
                    producer => this.setPlatform(producer.body),
                    error => console.log('Error: ' + error)
                 );
            }
        });
    }

    setPlatform(platform){
        this.producerForm = this.fb.group(platform);
    }    

    onReturn(){
        this.router.navigate(['/administrator/producers'], {queryParams: {}});
    }

    onSubmit(){
        this.producerService.update(this.idProducer,this.producerForm.value)
                 .subscribe(
                    result =>  {  
                    if ( result ) {
                        this.msg = 'Se editó el fabricante';
                        $('#alert-success').show();
                        setTimeout(function() {
                            $('#alert-success').hide();       
                        }, 2000);
                    } else {
                        this.msg = 'Ocurrio un error al editar';
                        $('#alert-danger').show();
                        setTimeout(function() {
                             $('#alert-danger').hide();       
                        }, 2000);
                    }
                });
    }
}
