import { Component, OnInit }         from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { ProducerService }                 from '../services/producers.service';
import { Producer }                     from '../producers.model';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-platform',
    template    : template,
    styles      : [ styles ],
    providers   : [ ProducerService ]
})

export class ProducersCreate implements OnInit {
    producer: Producer;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    producerForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb:FormBuilder, private router: Router, private http: Http, private producerService: ProducerService){
        this.producer = new Producer(0,'',false);
        this.title = 'Agregar Fabricante';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
         this.producerForm = this.fb.group({
            name: '',
            status: '' ,
            administrator_Id:1
        });
    }

    producerCreate(){         
      this.producerService.create( this.producerForm.value ).subscribe((result) => {
        if ( result ) {
            this.onReturn();
        } else {
            this.msg = 'Ocurrio un error al registrar';
            $('#alert-danger').show();
            setTimeout(function() {
                 $('#alert-danger').hide();       
            }, 2000);
        }
      })
    }

    onReturn(){
      this.router.navigate(['/administrator/producers'], {queryParams: {}});
    }
}
