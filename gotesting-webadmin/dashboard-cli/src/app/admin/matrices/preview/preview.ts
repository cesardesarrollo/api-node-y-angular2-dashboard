import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }                  from 'angular2-jwt';
import { MatrixService }                 		from '../services/matrices.service';
import { Matrix }                        		from '../matrix.model';
import { Router, ActivatedRoute }       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;
const styles    = require('./preview.css');
const template  = require('./preview.html');

@Component({
    selector    : 'preview-matrix',
    template    : template,
    styles      : [ styles ],
    providers   : [ MatrixService ]
})

export class MatricesPreview implements OnInit {
    
    matrix: 	Matrix;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
	idMatrix:   number;
	requeriments = [];

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private route: ActivatedRoute,private matrixService: MatrixService){
        window.scrollTo(0, 0);
        this.matrix = new Matrix(0, null, '', '', '', '');
        this.title = 'Matriz de purebas';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.getMatriz();
        this.getReqs();
    }
    
    ngOnInit(){
    }

    getMatriz(){
        this.route.params.subscribe(
        params => {
            if(params['id'] !== undefined) {

                this.idMatrix = params['id'];
                this.matrixService.getById(this.idMatrix).subscribe(
                	matrix => this.matrix = matrix.body,
                	error => console.log('Error: ' + error)
                );
            }
        });
    }

    getReqs(){
        this.matrixService.getReqs(this.idMatrix).subscribe(
			requeriments => this.requeriments = requeriments.body,
			error => console.log('Error: ' + error)
		);
    }

    onReturn(){
        this.router.navigate(['/administrator/matrices'], {queryParams: {}});
    }

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}