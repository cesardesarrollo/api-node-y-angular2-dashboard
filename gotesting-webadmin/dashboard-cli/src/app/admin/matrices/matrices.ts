import { Component,ElementRef,Renderer }  	from '@angular/core';
import { Http, Headers }                	from '@angular/http';
import { AuthHttp, JwtHelper }				from 'angular2-jwt';
import { MatrixService }                 	from './services/matrices.service';
import { Matrix }                        	from './matrix.model';
import { Router }                       	from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;
const styles    = require('./matrices.css');
const template  = require('./matrices.html');

@Component({
    selector    : 'matrices',
    template    : template,
    styles      : [ styles ],
    providers   : [ MatrixService ]
})

export class Matrices {
    
    matrix: Matrix;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    idMatrix: 	number;
    index: number;
    matrices = [];

    constructor(private router: Router,
                private http: Http,
                private matrixService: MatrixService,
                private el: ElementRef,
                private render: Renderer,
                private fb: FormBuilder){

        this.title = 'Matrices de pruebas';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
        this.renderData();
    }

    renderData(){
        this.matrixService.getAll().subscribe((result) => {
			if ( result.status ) {
				this.matrices = result.body;
				let array = [];
			}
		})
    }

    showModal(event, idMatrix, i){
      	this.index = i;
      	this.idMatrix = idMatrix;
      	event.preventDefault();
      	$("#modal-deactive").modal({
        	show  : true,
        	keyboard : false
      	});
      	$(".modal-backdrop").css( "z-index", "-1" );
    }

	delete(idMatrix){
        this.matrixService.update(idMatrix, { deleted: 1 }).subscribe(
            result =>  {
            	if ( result ) {
                  	this.matrices.splice(this.index, 1);
              	} else {
                	this.showAlert('warning', 'fa fa-warning', 'Error al intentar borrar el registro. intente mas tarde');
              	}
        })
	}

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}







