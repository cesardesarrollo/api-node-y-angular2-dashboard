import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../../../common/headers';
import { environment } 	  from '../../../../environments/environment';
import { Observable }     from 'rxjs/Rx';
import { Router }			from '@angular/router';

const baseUrl = environment.base_url_api;

@Injectable()
export class MatrixService {

	constructor( private http: Http, private router: Router){}

	create( formData ) {

		let xhr = new XMLHttpRequest();
		
		xhr.open('POST', baseUrl + '/matriz_tests/new', true);
		xhr.setRequestHeader("enctype", "multipart/form-data");
		xhr.setRequestHeader("Cache-Control", "no-cache");
		xhr.setRequestHeader("token", localStorage.getItem('id_token'));
		xhr.setRequestHeader("Accept", 'application/json');
		xhr.withCredentials = true;

		xhr.onreadystatechange = function () {
            if (xhr.status === 200) {
				let result = JSON.parse(xhr.responseText);
				$("#created").val(result.response.id);
				$("#created").click();
			} else {
				return false;
			}
		}
		xhr.send( formData );
	}

	getAll(){
		var filter = JSON.stringify({'where':{'deleted':0}});
		let msg;
		return this.http
			.get(
				baseUrl + '/matriz_tests?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getById(id){
		let msg;
		return this.http
			.get(
				baseUrl + '/matriz_tests/'+id,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	update(id, object){
		//object.media_Id = null;
		//delete object.media_id;
		let body = JSON.stringify(object);
		let msg;
		return this.http
			.put(
				baseUrl + '/matriz_tests/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}
	
	createReq( idMatrix, data ) {
		data.matriz_test_Id = idMatrix;
		let body = JSON.stringify( data );
		return this.http
			.post(
				baseUrl + '/matriz_test_requirements',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {;
				if( res.status === 200 ) {
					return res;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}

	updateReq(id, object){
		let body = JSON.stringify(object);
		let msg;
		return this.http
			.put(
				baseUrl + '/matriz_test_requirements/'+id,
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return true;
				} else {
					return false;
				}
			})
			.catch(this.handleError);
	}
	
	getFile( mediaId ){
		var filter = JSON.stringify({ 'where':{'id':mediaId} });
		return this.http
			.get(
				baseUrl + '/media?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	getReqs(idMatrix){
		var filter = JSON.stringify({ 'where':{'matriz_test_Id':idMatrix, 'deleted':0} });
		let msg;
		return this.http
			.get(
				baseUrl + '/matriz_test_requirements?filter='+filter,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.status === 200 ) {
					return {status: true, body: res.json() };
				} else {
					return {status: false, body: {}};
				}
			});
	}

	private handleError (error: any) {
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	return Observable.throw(error);
  	}

}