import { Component, OnInit }			from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }			from 'angular2-jwt';
import { MatrixService }				from '../services/matrices.service';
import { Matrix }						from '../matrix.model';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-matrix',
    template    : template,
    styles      : [ styles ],
    providers  :  [ MatrixService ]
})

export class MatricesCreate implements OnInit {

    matrix: 	Matrix;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    matrixForm: FormGroup;
    idMatrix:   number;

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private matrixService: MatrixService){

        this.matrix = new Matrix(0, null, '', '', '', '');
        this.title = 'Crear matriz de pruebas';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
        this.matrixForm = this.fb.group({
            name: '',
            name_client: '',
            name_project: '',
            version: '',
            description: '',
            createdAt: '',
            file:[]
        });
    }

    onCreate(){
    	let formData: FormData = new FormData();
    	let xhr = new XMLHttpRequest();
		/*let fileList: FileList = $("#file")[0].files;

		if(fileList.length > 0) {
			let file: File = fileList[0];
			formData.append('file', file, file.name);
		}*/

		var date = new Date();
    	formData.append('name', this.matrixForm.value.name);
    	formData.append('name_project', this.matrixForm.value.name_project);
    	formData.append('name_client', this.matrixForm.value.name_client);
    	formData.append('description' ,this.matrixForm.value.description);
    	formData.append('version', this.matrixForm.value.version);
    	formData.append('createdAt', date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate());
        this.matrixService.create(formData);
    	//formData.append('file', $("#file")[0].files);
    }

	created(){
  		this.redirectEdit();
    }

    redirectEdit(){
		this.router.navigate(['/administrator/matrices/edit/'+$("#created").val()], {queryParams: {}});
    }

    onReturn(){
        this.router.navigate(['/administrator/matrices'], {queryParams: {}});
    }

    showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}