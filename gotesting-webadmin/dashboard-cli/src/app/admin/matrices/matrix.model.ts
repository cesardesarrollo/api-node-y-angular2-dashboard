export class Matrix {
  constructor(
  	public id: number,
    public status: boolean,
    public name: string,
    public createdAt: string,
    public updatedAt: string,
    public description: string
  ) {  }
}