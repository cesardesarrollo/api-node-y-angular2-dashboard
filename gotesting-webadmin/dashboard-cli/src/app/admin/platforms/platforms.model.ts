export class Platform {
  constructor(
  	public id: number,
    public status: boolean,
    public name: string,
  ) {  }
}

/* {
    "name": "string",
    "description": "string",
    "category": "string",
    "producer": "string",
    "administrator_Id": 1,
    "status": true,
    "createdAt": "2016-09-06T00:00:00.000Z",
    "updatedAt": "2016-09-06T00:00:00.000Z",
    "id": 3
  },*/