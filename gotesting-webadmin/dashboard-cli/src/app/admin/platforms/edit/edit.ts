import { Component }                    		from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }                  from 'angular2-jwt';
import { Platform }                     		from '../platforms.model';
import { PlatformService }              		from '../services/platforms.service';
import { Router,ActivatedRoute }				from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;

const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-platform',
    template    : template,
    styles      : [ styles ],
    providers   : [ PlatformService ]
})

export class PlatformsEdit {
    platform: Platform;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    idPlatform: number;
    msg:        string;
    platformForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb:FormBuilder, private router: Router, private http: Http, private route: ActivatedRoute,private platformService: PlatformService){
        window.scrollTo(0, 0);
        this.platform = new Platform(0,false,'');
        this.title = 'Editar Plataforma';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.getPlatforms();
    }

    ngOnInit(){
         this.platformForm = this.fb.group({
            name: '',
            producer: '',
            status: false 
        });
    }

    getPlatforms(){
        this.route.params.subscribe(params => {
            if(params['id'] !== undefined) {
                this.idPlatform = params['id'];
                console.log(this.idPlatform);
                this.platformService.getById(this.idPlatform)
                 .subscribe(
                    platform => this.setPlatform(platform.body),
                    error => console.log('Error: ' + error)
                 );
            }
        });
    }

    setPlatform(platform){
        this.platformForm = this.fb.group(platform);
    }    

    onReturn(){
        this.router.navigate(['/administrator/platforms'], {queryParams: {}});
    }

    onSubmit(){
        this.platformService.update(this.idPlatform,this.platformForm.value)
                 .subscribe(
                    result =>  {  
                    if ( result ) {
                        this.msg = 'Se editó la plataforma';
                        $('#alert-success').show();
                        setTimeout(function() {
                            $('#alert-success').hide();       
                        }, 2000);
                    } else {
                        this.msg = 'Ocurrio un error al editar';
                        $('#alert-danger').show();
                        setTimeout(function() {
                             $('#alert-danger').hide();       
                        }, 2000);
                    }
                });
    }
}
