import { Component, OnInit }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { PlatformService }                 from '../services/platforms.service';
import { Platform }                     from '../platforms.model';
import { Router }                       from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-platform',
    template    : template,
    styles      : [ styles ],
    providers   : [ PlatformService ]
})

export class PlatformsCreate implements OnInit {
    platform: Platform;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    platformForm: FormGroup;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb:FormBuilder, private router: Router, private http: Http, private platformService: PlatformService){
        this.platform = new Platform(0,false,'');
        this.title = 'Agregar Plataforma';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
         this.platformForm = this.fb.group({
            name: '',
            status: '' 
        });
    }

    platformCreate(){         
      this.platformService.create( this.platformForm.value ).subscribe((result) => {
        if ( result ) {
            this.onReturn();
        } else {
            this.msg = 'Ocurrio un error al registrar';
            $('#alert-danger').show();
            setTimeout(function() {
                 $('#alert-danger').hide();       
            }, 2000);
        }
      })
    }

    onReturn(){
      this.router.navigate(['/administrator/platforms'], {queryParams: {}});
    }
}
