import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }			from 'angular2-jwt';
import { DeviceService }              	from './services/devices.service';
import { Device }                     	from './devices.model';
import { Router }                       from '@angular/router';

declare var $:any;
const styles    = require('./devices.css');
const template  = require('./devices.html');

@Component({
    selector    : 'devices',
    template    : template,
    styles      : [ styles ],
    providers   : [ DeviceService ]  
})

export class AdminDevices {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    index: number;
    Device: Device;
    idDevice: number;
    devices = [];

    constructor(private router: Router, private http: Http, private deviceService: DeviceService){
        this.title = 'Catálogo Dispositivos';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt =  this.jwtHelper.decodeToken(this.jwt);
    }
    
    ngOnInit(){
        this.getData();
    }

    getData(){
        this.deviceService.getAll().subscribe((result) => {
            if ( result.status ) {
                this.devices = result.body;
            }
        })
    }

	showModal(event, idDevice, i){
		
      	this.index = i;
      	this.idDevice = idDevice;
      	event.preventDefault();

      	$("#modal-deactive").modal({
        	show  : true,
        	keyboard : false
      	});
      	$(".modal-backdrop").css( "z-index", "-1" );
    }

    delete(idDevice){
        this.deviceService.update(idDevice, { deleted: 1 })
        .subscribe(
            result =>  {
            	if ( result ) {
                  	this.devices.splice(this.index, 1);
              	} else {
                	this.showAlert('warning', 'fa fa-warning', 'Error al intentar borrar el registro. intente mas tarde');
                	$('#alert-danger').show();
              	}
        })
	}

	showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}