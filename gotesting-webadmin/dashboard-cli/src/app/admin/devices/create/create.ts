import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }					from 'angular2-jwt';
import { DeviceService }              			from '../services/devices.service';
import { Device }                     			from '../devices.model';
import { Router }                       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-device',
    template    : template,
    styles      : [ styles ],
    providers   : [ DeviceService ]
})

export class AdminDevicesCreate implements OnInit {
    device: Device;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    deviceForm: FormGroup;
    platforms = [];
    producers = [];
    categories = [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private deviceService: DeviceService){
        this.device = new Device(0,false,'','','');
        this.title = 'Agregar Dispositivo';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
        this.deviceForm = this.fb.group({
            name: '',
            dev_platform_Id: '',
            dev_category_Id: '',
            dev_producer_Id: '',
            status: ''
        });

		this.deviceService.getPlatforms().subscribe((result) => {
            this.platforms = result.body;
        });

        this.deviceService.getCategories().subscribe((result) => {
            this.categories = result.body;
        });

        this.deviceService.getProducers().subscribe((result) => {
            this.producers = result.body;
        })
    }

    onCreate(){         
      this.deviceService.create( this.deviceForm.value ).subscribe((result) => {
        if ( result ) {
            this.onReturn();
        } else {
			this.showAlert('warning', 'fa fa-warning', 'Error al intentar borrar el registro. intente mas tarde');
        }
      })
    }

    onReturn(){
      	this.router.navigate(['/administrator/devices'], {queryParams: {}});
    }

	showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}
