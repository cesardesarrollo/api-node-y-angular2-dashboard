import { Component }                    		from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }					from 'angular2-jwt';
import { DeviceService }              			from '../services/devices.service';
import { Device }                     			from '../devices.model';
import { Router,ActivatedRoute }				from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;

const styles    = require('./edit.css');
const template  = require('./edit.html');

@Component({
    selector    : 'edit-device',
    template    : template,
    styles      : [ styles ],
    providers   : [ DeviceService ]
})

export class AdminDevicesEdit {
    device: Device;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    idDevice:   number;
    deviceForm: FormGroup;
    platforms = [];
    producers = [];
    categories = [];
    jwtHelper: JwtHelper = new JwtHelper();
    
    constructor(private fb:FormBuilder, private router: Router, private http: Http, private route: ActivatedRoute,private deviceService: DeviceService){
        window.scrollTo(0, 0);
        this.device = new Device(0,false,'','','');
        this.title = 'Editar Dispositivo';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.getDevice();
    }

    ngOnInit(){
        this.deviceForm = this.fb.group({
            name: '',
            dev_platform_Id: '',
            dev_category_Id: '',
            dev_producer_Id: '',
            status: ''
        });

		this.deviceService.getPlatforms().subscribe((result) => {
            this.platforms = result.body;
        });

        this.deviceService.getCategories().subscribe((result) => {
            this.categories = result.body;
        });

        this.deviceService.getProducers().subscribe((result) => {
            this.producers = result.body;
        })
    }

    getDevice(){
        this.route.params.subscribe(params => {
            if(params['id'] !== undefined) {
                this.idDevice = params['id'];
                this.deviceService.getById(this.idDevice)
                .subscribe(
                   	device => this.setDevice(device.body),
                    error => console.log('Error: ' + error)
                );
            }
        });
    }

    setDevice(device){
        this.deviceForm = this.fb.group(device);
    }

	onUpdate(){
        this.deviceService.update(this.idDevice, this.deviceForm.value).subscribe(
            result =>  {
                if ( result ) {
					this.showAlert('success', 'fa fa-check', 'Se modifico el registro correctamente');
					this.onReturn();
				} else {
					this.showAlert('warning', 'fa fa-warning', 'Error al editar el registro. intente mas tarde');
				}
		});
    }

    onReturn(){
        this.router.navigate(['/administrator/devices'], {queryParams: {}});
    }

	showAlert(type, icon, msg){
    	$("#alertify").addClass('active ' + type);
		$('#alertify div.icon').html('<i class="' + icon + '"></i>');
		$('#alertify div.msg span').html(msg);
		setTimeout(function(){
			$("#alertify").removeClass('active ' + type);
		}, 3500);
    }
}
