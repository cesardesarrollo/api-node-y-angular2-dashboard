export class Device {
  constructor(
  	public id: number,
    public status: boolean,
    public producer: string,
    public name: string,
    public category: string
  ) {  }
}

/* {
    "name": "string",
    "description": "string",
    "category": "string",
    "producer": "string",
    "administrator_Id": 1,
    "status": true,
    "createdAt": "2016-09-06T00:00:00.000Z",
    "updatedAt": "2016-09-06T00:00:00.000Z",
    "id": 3
  },*/