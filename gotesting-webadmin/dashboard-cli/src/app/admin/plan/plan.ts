import { Component }                    from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { Router }                       from '@angular/router';

const styles    = require('./plan.css');
const template  = require('./plan.html');

@Component({
    selector    : 'plan',
    template    : template,
    styles      : [ styles ]
})

export class Plan {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private router: Router, private http: Http){
        this.title = 'Plans view';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    logout(){
        localStorage.removeItem('id_token');
        this.router.navigate(['/login']);
    }
}