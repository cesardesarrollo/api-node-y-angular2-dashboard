export class Exam {
  	constructor (
  		public id: number,
    	public name: string,
    	public description: string,
    	public date_start: Date,
    	public date_end: Date,
    	public time: number,
    	public attemps: boolean,
    	public score: number
  	) {}
}
