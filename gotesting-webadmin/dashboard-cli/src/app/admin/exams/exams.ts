import { Component, OnInit } 			from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }			from 'angular2-jwt';
import { ExamService }              	from './services/exams.service';
import { Exam }                     	from './exams.model';
import { Router }                       from '@angular/router';

declare var $:any;

const styles    = require('./exams.css');
const template  = require('./exams.html');

@Component({
  	selector	: 'exams',
    template    : template,
    styles      : [ styles ],
    providers   : [ ExamService ]  
})

export class Exams implements OnInit {

    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    jwtHelper: JwtHelper = new JwtHelper();
    exams = [];
    msg = '';

  	constructor( private examService: ExamService) { 
  		this.title = 'Catálogo Examenes';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt =  this.jwtHelper.decodeToken(this.jwt);
  	}

	ngOnInit(){
        this.getData();
    }

    getData(){
        this.examService.getAll().subscribe((result) => {
            if ( result.status ) {
                this.exams = result.body;
            } else {
                this.msg = 'No se encontraron datos';
            }
        })
    }

}
