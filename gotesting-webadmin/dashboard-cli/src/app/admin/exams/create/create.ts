import { Component, OnInit }					from '@angular/core';
import { Http, Headers }                		from '@angular/http';
import { AuthHttp, JwtHelper }					from 'angular2-jwt';
import { ExamService }              			from '../services/exams.service';
import { Exam }                     			from '../exams.model';
import { Router }                       		from '@angular/router';
import { FormBuilder, FormGroup, FormControl } 	from '@angular/forms';

declare var $:any;
const styles    = require('./create.css');
const template  = require('./create.html');

@Component({
    selector    : 'create-exam',
    template    : template,
    styles      : [ styles ],
    providers   : [ ExamService ]
})

export class ExamsCreate implements OnInit {
    exam: Exam;
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: string;
    msg:        string;
    examForm: FormGroup;
    platforms = [];
    producers = [];
    categories = [];
    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private fb: FormBuilder, private router: Router, private http: Http, private examService: ExamService){
        this.exam = new Exam(0, '', '', null, null, 0, false, 0);
        this.title = 'Agregar examen';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.msg = '';
    }

    ngOnInit(){
        this.examForm = this.fb.group({
            name: '',
            description: '',
            date_start: '',
            date_end: '',
            time: '',
            attemps: '',
            score: ''
        });

		this.examService.getPlatforms().subscribe((result) => {
            this.platforms = result.body;
        });

        this.examService.getCategories().subscribe((result) => {
            this.categories = result.body;
        });

        this.examService.getProducers().subscribe((result) => {
            this.producers = result.body;
        })
    }

    examCreate(){         
      this.examService.create( this.examForm.value ).subscribe((result) => {
        if ( result ) {
            this.onReturn();
        } else {
            this.msg = 'Ocurrio un error al registrar';
            $('#alert-danger').show();
            setTimeout(function() {
                 $('#alert-danger').hide();       
            }, 2000);
        }
      })
    }

    onReturn(){
      	this.router.navigate(['/administrator/exams'], {queryParams: {}});
    }
}
