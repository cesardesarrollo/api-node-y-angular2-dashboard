import { Component,ElementRef,ViewChild,Renderer }         from '@angular/core';
import { Http, Headers }                from '@angular/http';
import { AuthHttp, JwtHelper }                     from 'angular2-jwt';
import { contentHeaders }               from '../common/headers';
import { Router }                       from '@angular/router';

const styles    = require('./admin.css');
const template  = require('./administrator.html');

@Component({
    selector    : 'administrator',
    template    : template,
    styles      : [ styles ]
})

export class Administrator {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: any;
    jwtHelper: JwtHelper = new JwtHelper();

    constructor( private router: Router, private http: Http,el: ElementRef,render: Renderer) { 
        this.title = 'Welcome administrator';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
        this.userName = this.decodedJwt.user_fullName;
        render.listen(el.nativeElement, 'click', (event) => {
           
        });
    }

    logout(){
        localStorage.removeItem('id_token');
        this.router.navigate(['/login']);
    }

    optionToggled(){
        console.log("toggled....");
    }

    showMenu(){
        $('#menu-trigger').toggleClass('open');
        $('#sidebar').toggleClass('toggled');     
    }

    toLocation(route){
        this.showMenu(); 
        this.router.navigate(['/administrator/'+route]);
    }
}
