import { Component, OnInit }                         from '@angular/core';
import { Http, Headers }                     from '@angular/http';
import { contentHeaders }                    from '../common/headers';
import { LoginService }                      from './login.service';
import { AuthService }                       from '../common/auth.service';
import { Router }                            from '@angular/router';
import { AuthHttp, JwtHelper }                          from 'angular2-jwt';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles   = require('./login.css');
const template = require('./login.html');

@Component({
  selector   : 'login',
  template   : template,
  styles     : [ styles ]
})

export class Login  implements OnInit {
  loginForm: FormGroup;
  resetForm: FormGroup;
  msg: string;
  msgPassword: string;
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private fb: FormBuilder, private router: Router, private loginService: LoginService, private authService: AuthService) { }
   
   ngOnInit(){
       this.msgPassword = '';
         this.loginForm = this.fb.group({
            email: '',
            password: '' 
        });
        this.resetForm = this.fb.group({
          email: ''
        })
    }

  sanitizeString(str){
    return str.trim();
  }  
  
  //ONSUBMIT LOGGIN
  onSubmit(){
    let passwd = this.sanitizeString(this.loginForm.value.password);
    let email = this.sanitizeString(this.loginForm.value.email);
    this.authService.signinUser( email , passwd ).subscribe((result) => {
        var userData = this.jwtHelper.decodeToken(result.token);
      if ( result.success ) {
        $(".btn-login").prop('disabled', true);
        $(".btn-login").text('Procesando...');
        console.log("success");
        if( userData.user_role === 1 ) {
          console.log("to admin..");
          this.router.navigate(['/administrator']);
        } else if ( userData.user_role === 2) {
          console.log("to tester..");
          this.router.navigate(['/tester']);
        }
      } else {
        $(".alert-login").slideToggle('fast');
        this.msg = result.msg;
        setTimeout(function() {
          $("#username").val('');
          $("#username").focus();
          $("#password").val('');
          $(".btn-login").prop('disabled', false);
          $(".btn-login").text('Login');
          $(".alert-login").slideToggle('fast');
          this.msg = '';
        }, 2000);        
      }
    }, (error) =>{
      this.msg = error.json().error.message;
      console.log(error.json().error);
      $(".alert-login").slideToggle('fast');
      setTimeout(function() {
          $(".alert-login").slideToggle('fast');
          this.msg = '';
        }, 2000);  
    }
    );
  }

  //SHOW PASSWORD MODAL
  forgotPassword(event){
    event.preventDefault();
    $("#modal-password").modal({
      show  : true,
      backdrop : 'static',
      keyboard : false
    })
  }

  //RESET PASSWORD
  resetPassword(){
    this.loginService.ressetPassword( this.resetForm.value.email ).subscribe((result)=> {
      console.log(result);
      if ( result.response.success ) {
          this.msgPassword = result.response.message;
          $("#txt__Email").val('');
          $(".alert-password").slideToggle();
          $(".alert-password").addClass('alert-success');   
      } else {
         this.msgPassword = result.response.message;
         $(".alert-password").slideToggle();
         $(".alert-password").addClass('alert-danger');
      }
      setTimeout(function() {
        $(".alert-password").slideToggle();
        $("#modal-password").modal('hide');        
      }, 4000);
    })
  }
}