import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../common/headers';
import { environment } 	  from '../../environments/environment';

const baseUrl = environment.base_url_api;

@Injectable()
export class LoginService {

	private loggedIn = false;

	constructor( private http: Http){
		this.loggedIn = !!localStorage.getItem('id_token');
	}

	//LOGIN METHOD
	authenticate( email, password ) {
		let body = JSON.stringify({ email, password });
		let msg;
		return this.http
			.post(
				baseUrl + '/users/login',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				if( res.json().ok ) {
					localStorage.setItem('id_token', res.json().response.token );
					this.loggedIn = true;
					msg = { success: res.json().response.success, token : res.json().response.token, msg: res.json().response.message };
				} else {
					msg = { success: res.json().response.success, token : res.json().response.token, msg: res.json().response.message  };
				}
				return msg;
			});

	}
	//RESETPASSWORD
	ressetPassword(email){
		let body = JSON.stringify({ email });
		let msg;
		return this.http
			.post(
				baseUrl + '/users/reset/password',
				body,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				 msg = res.json();
				 return msg;
			});
	}
	//LOGOUT METHOD
	logout(){
		localStorage.removeItem('id_token');
		this.loggedIn = false;
	}

	//CHECK AUTH
	isLoggedIn() {
		return this.loggedIn;
	}
}