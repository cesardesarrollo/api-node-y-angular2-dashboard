import { Injectable }       from '@angular/core';
import { Http }             from '@angular/http';
import { contentHeaders }   from '../common/headers';
import { environment }      from '../../environments/environment';
import { Observable }       from 'rxjs/Rx';

const baseUrl = environment.base_url_api;

@Injectable()
export class testerService {

    //BUILD CLASS CONSTRUCTOR
    constructor(private http: Http){}

    getTester(tester_id){
        return this.http
            .get(baseUrl + '/users/' + tester_id, { headers : contentHeaders })
            .map( (tester) =>  tester.json())
            .catch( this.handleError );
    }


    //HANDLE ERROR
    private handleError (error: any) {
		//console.log(error);
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}


}