import { Component }        from '@angular/core';
import { Http, Headers}     from '@angular/http';
import { AuthHttp, JwtHelper}from 'angular2-jwt';
import { Router }           from '@angular/router';
import { testerService }    from './tester.service';
import { AuthService }      from '../common/auth.service';

const styles    = require('./tester.css');
const template  = require('./tester.html');

@Component({
    selector    : 'tester',
    template    : template,
    styles      : [ styles ],
    providers   : [ testerService ]
})

export class Tester {
    title:      string;
    userName:   string;
    userRole:   string;
    jwt:        string;
    decodedJwt: any;
    testerAvatar: string;
    jwtHelper: JwtHelper = new JwtHelper();
    user: any = {};

    constructor(private router: Router, private http: Http, private ts: testerService, private as: AuthService){
        this.title = 'Welcome Tester';
        this.jwt = localStorage.getItem('id_token');
        this.decodedJwt = this.jwtHelper.decodeToken(this.jwt);
    }

    ngOnInit(){
        //this.getTesterData(this.decodedJwt.user_id);
        this.testerAvatar = this.decodedJwt.user_avatar;
        if( this.decodedJwt.user_fullName == "null null") {
            this.userName = "Usuario";
        } else {
            this.userName = this.decodedJwt.user_fullName;
        }
    }

    logout(){
        this.as.logout();
    }

    private getTesterData(tester_id){
        this.ts.getTester(tester_id).subscribe( user => { 
            this.user = user
            this.jwt  = user.token;
            this.decodedJwt = this.jwtHelper.decodeToken( this.jwt ); 
        });
    }
}