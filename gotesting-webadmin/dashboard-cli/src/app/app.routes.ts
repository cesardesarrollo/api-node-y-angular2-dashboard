import { RouterModule, Routes } from '@angular/router';
import { Home } 		    from './home';
import { Login } 		    from './login';
import { Signup } 		  from './signup';
/*
 *  ADMINISTRATOR ROUTES
 *  =======================
 */
import { Administrator }from './administrator';
import { Test }         from './admin/test';
import { Capacitation } from './admin/capacitation';
import { Certification }from './admin/certification';
import { Plan }         from './admin/plan';
import { Levels }       from './admin/levels';
import { LevelsCreate } from './admin/levels/create';
import { LevelsEdit }   from './admin/levels/edit';
import { Lessons }      from './admin/lessons';
import { LessonsCreate }from './admin/lessons/create';
import { LessonsEdit }  from './admin/lessons/edit';
import { Categories } from './admin/categories';
import { CategoriesCreate } from './admin/categories/create';
import { CategoriesEdit } from './admin/categories/edit';
import { Producers } from './admin/producer';
import { ProducersCreate } from './admin/producer/create';
import { ProducersEdit } from './admin/producer/edit';
import { Platforms }    from './admin/platforms';
import { PlatformsCreate } from './admin/platforms/create';
import { PlatformsEdit }from './admin/platforms/edit';
import { Courses }      from './admin/courses';
import { CoursesCreate }      from './admin/courses/create';
import { CoursesEdit }      from './admin/courses/edit';
import { Agreements }     from './admin/agreements';
import { Deal }         from './admin/agreements/deal';
import { DealEdit }     from './admin/agreements/deal/edit';
import { Terms }        from './admin/agreements/terms';
import { TermsEdit }    from './admin/agreements/terms/edit';
import { Matrices }      from './admin/matrices';
import { MatricesCreate } from './admin/matrices/create';
import { MatricesEdit }   from './admin/matrices/edit';
import { MatricesPreview }   from './admin/matrices/preview';
import { AdminDevices }      from './admin/devices';
import { AdminDevicesCreate }      from './admin/devices/create';
import { AdminDevicesEdit }      from './admin/devices/edit';
import { Exams }      from './admin/exams';
import { ExamsCreate } from './admin/exams/create';
import { ExamsQuestion } from './admin/exams/question';
import { Rins }      from './admin/rins';
import { RinsCreate } from './admin/rins/create';
import { RinSeverity }      from './admin/rin-severity';
import { RinSeverityCreate } from './admin/rin-severity/create';
import { RinStatus }      from './admin/rin-status';
import { RinStatusCreate } from './admin/rin-status/create';
import { RinError }      from './admin/rin-error';
import { RinErrorCreate } from './admin/rin-error/create';
import { Pricing } from './admin/pricing';
import { PricingCreate } from './admin/pricing/create';

//import { ATerms }        from '.admin/settings/terms';
import { Task }         from './admin/task';
import { TaskCreate }         from './admin/task/create';
import { TaskEdit }         from './admin/task/edit';
import { Subscription } from './admin/subscription';
import { AdminHome }    from './admin/home';
import { Account }      from './account';
import { Tester }       from './tester';
/*
 *  TESTER ROUTES
 *  =======================
 */
import { Devices }      from './_tester/devices';
import { CreateDevice } from './_tester/devices/create';
import { EditDevice }   from './_tester/devices/edit';
import { TCapacitation }from './_tester/capacitation';
import { TCertification}from './_tester/certification';
import { TCourseDetail }from './_tester/capacitation/course-detail';
import { TLessonDetail }from './_tester/capacitation/lesson-detail';
import { MyCourses }    from './_tester/capacitation/my-courses';
import { Profile }      from './_tester/profile';
import { TTasks }       from './_tester/tasks';
import { TFinantial }   from './_tester/finantial';
import { TesterHome }   from './_tester/home';

import { AuthGuard } 	  from './common/auth.guard';

const routes: Routes = [
  { path: '',       component:  Login },
  { path: 'login',  component: Login },
  { path: 'signup', component: Signup },
  { path: 'account/activation/:e/:c',  component: Account },  
  { path: 'home',   component: Home/*, canActivate: [AuthGuard]*/ },

  { path: 'administrator',   component: Administrator, /*canActivate: [AuthGuard],*/ 
    children: [
     { path: '', redirectTo: 'home' },
     { path: 'home', component: AdminHome },
     { path: 'test', component: Test }, 
     { path: 'capacitation', component: Capacitation },
     { path: 'certification', component: Certification },
     { path: 'task', component: Task },
     { path: 'task/create', component: TaskCreate },
     { path: 'task/edit/:id', component: TaskEdit },
     { path: 'subscription', component: Subscription },
     { path: 'plans', component: Plan },
     { path: 'levels', component: Levels },
     { path: 'levels/create', component: LevelsCreate },
     { path: 'levels/edit/:id', component: LevelsEdit },
     { path: 'lessons', component: Lessons },
     { path: 'lessons/create', component: LessonsCreate },
     { path: 'lessons/edit/:id', component: LessonsEdit },
     { path: 'categories', component: Categories },
     { path: 'categories/create', component: CategoriesCreate },
     { path: 'categories/edit/:id', component: CategoriesEdit },
     { path: 'producers', component: Producers },
     { path: 'producers/create', component: ProducersCreate },
     { path: 'producers/edit/:id', component: ProducersEdit },
     { path: 'platforms', component: Platforms },
     { path: 'platforms/create', component: PlatformsCreate },
     { path: 'platforms/edit/:id', component: PlatformsEdit },
     { path: 'courses', component: Courses },
     { path: 'courses/create', component: CoursesCreate },
     { path: 'courses/edit/:id', component: CoursesEdit },
     { path: 'agreements', component: Agreements },
     { path: 'agreements/edit/:id', component: TermsEdit },
     { path: 'matrices', component: Matrices },
     { path: 'matrices/create', component: MatricesCreate },
     { path: 'matrices/edit/:id', component: MatricesEdit },
     { path: 'matrices/preview/:id', component: MatricesPreview },
	 { path: 'devices', component: AdminDevices },
     { path: 'devices/create', component: AdminDevicesCreate },
     { path: 'devices/edit/:id', component: AdminDevicesEdit },
	 { path: 'exams', component: Exams },
     { path: 'exams/create', component: ExamsCreate },
     { path: 'exams/question/:id', component: ExamsQuestion },
     { path: 'rins', component: Rins },
     { path: 'rins/create', component: RinsCreate },
     { path: 'rin-severity', component: RinSeverity },
     { path: 'rin-severity/create', component: RinSeverityCreate },
     { path: 'rin-status', component: RinStatus },
     { path: 'rin-status/create', component: RinStatusCreate },
     { path: 'rin-error', component: RinError },
     { path: 'rin-error/create', component: RinErrorCreate },
     { path: 'pricing', component: Pricing },
     { path: 'pricing/create', component: PricingCreate },
    ]
  },

   /*  TESTER ROUTES
   *  ================ 
   */
  { path: 'tester', component: Tester, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'home', canActivate: [AuthGuard]},
      { path: 'home', component: TesterHome, canActivate: [AuthGuard] },
      { path: 'devices', component: Devices, canActivate: [AuthGuard] },
      { path: 'devices/new', component: CreateDevice, canActivate: [AuthGuard]},
      { path: 'devices/edit/:id', component: EditDevice, canActivate: [AuthGuard]},
      { path: 'courses', component: TCapacitation, canActivate: [AuthGuard]},
      { path: 'courses/detail/:id', component: TCourseDetail, canActivate: [AuthGuard]},
      { path: 'courses/detail/:course_id/lesson/:lesson_id', component: TLessonDetail, canActivate: [AuthGuard]},
      { path: 'courses/:username', component: MyCourses, canActivate: [AuthGuard]},
      { path: 'certification', component: TCertification, canActivate: [AuthGuard]},
      { path: 'profile', component: Profile, canActivate: [AuthGuard]},
      { path: 'tasks', component: TTasks, canActivate: [AuthGuard]},
      { path: 'finantial', component: TFinantial, canActivate: [AuthGuard]}
    ]
  },
  { path: '**',     component: Login }
];

export const routing = RouterModule.forRoot(routes);