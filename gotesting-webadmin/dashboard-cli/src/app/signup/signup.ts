import { Component,OnInit }                       from '@angular/core';
import { Http }                            from '@angular/http';
import { contentHeaders }                  from '../common/headers';
import { SignUpService }                   from './signup.service';
import { Router }                            from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

declare var $:any;

const styles   = require('./signup.css');
const template = require('./signup.html');

@Component({
  selector  : 'signup',
  template  : template,
  styles    : [ styles ],
  providers : [ SignUpService ]
})

export class Signup  implements OnInit  {
  //GLOBAL VARS
  signupForm: FormGroup;
  msg: string;
  text: string;
  title: string;

  constructor(private fb: FormBuilder, public router: Router, public http: Http, private sgService: SignUpService) {
    this.sgService.getTerms().subscribe( (result) => {
       if(result.status){
         this.title = result.body[0].title;
         this.text = result.body[0].description;
       }
    })
  }
   ngOnInit(){
         this.signupForm = this.fb.group({
            email: '',
            password: '' 
        });
    }

  //ONSUBMIT FORM
  signup(){    
    $(".btn-signup").prop('disabled', true);
    $(".btn-signup").text('Registrandome...');
    this.sgService.signup( this.signupForm.value.email, this.signupForm.value.password ).subscribe( (result) => {
      if ( result.response.success ) {      
        $('.alert-signup').slideToggle('fast');
        $('.alert-signup').addClass('alert-success');        
        this.msg =  "Activa tu cuenta desde el correo que enviamos ";
          $("#username").val('');
          $("#password").val('');
          $(".btn-signup").text('Registrarme');
          $(".btn-signup").prop('disabled', false);
          $("#agree").prop('checked', false);
          //$('.alert-signup').slideToggle('fast');
      } else {
        $('.alert-signup').slideToggle('fast');
        $('.alert-signup').addClass('alert-danger');
        this.msg = result.msg;
      }
    })
  }

  login(event) {
    event.preventDefault();
    this.router.navigate(['/login']);
  }

  //READ TERMS AND CONDITIONS MODAL
  readTermsAndConditions(event){
    event.preventDefault();
    console.log( $("#agree").is(':checked') );
    if( !$("#agree").is(':checked') ) {
      $("#agree").prop('checked', false)
      $(".btn-signup").prop('disabled', true);
    } else {
      $("#modal-terms").modal({
        show : true,
        backdrop : 'static',
        keyboard : false
      });
    }
  }

  acceptTerms(event){
    event.preventDefault;
    $("#agree").prop('checked', true);
    $(".btn-signup").prop('disabled', false);
    $("#modal-terms").modal('hide');
  }
}
