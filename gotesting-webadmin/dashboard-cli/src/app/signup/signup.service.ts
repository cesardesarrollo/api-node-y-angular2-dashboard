import { Injectable }     from '@angular/core';
import { Http  }          from '@angular/http';
import { contentHeaders } from '../common/headers';
import { environment } 	  from '../../environments/environment';

const baseUrl = environment.base_url_api;

@Injectable()
export class SignUpService {

    constructor( private http: Http ){ }

    //SIGNUP METHOD
    signup( email, password ){
        let body = JSON.stringify({ email, password });
        let msg;
        return this.http
            .post(
                baseUrl + '/users/new',
                body,
                { headers : contentHeaders }
            )
            .map( (res)=> {
                msg =  res.json();
                return msg;
            });
    }

    getTerms(){
         var filter = JSON.stringify({'where':{'type':'tyc'}});
        return this.http
            .get(
                baseUrl + '/agreements?filter='+filter,
                { headers : contentHeaders }
            )
            .map( (res) => {
                if( res.status === 200 ) {
                    return {status: true, body: res.json() };
                } else {
                    return {status: false, body: {}};
                }
            });
    }

}