import { Injectable } 	  from '@angular/core';
import { Http }			  from '@angular/http';
import { contentHeaders } from '../common/headers';
import { environment } 	  from '../../environments/environment';
import { Observable }     from 'rxjs/Rx';

const baseUrl = environment.base_url_api;

@Injectable()
export class AccountService {

	constructor( private http: Http){

	}

	//LOGIN METHOD
	activate( e, c) {
		let msg;
		return this.http
			.get(
				baseUrl + '/users/account/activation?e=' + e + '&c=' + c,
				{ headers : contentHeaders }
			)
			.map( (res) => {
				console.log(res.json());
				if( res.json().response.success ) {
					msg = { success: res.json().response.success, msg : res.json().response.message };
				} else {
					msg = { success: res.json().response.success, msg : res.json().response.message };
				}
				return msg;
			})
			.catch(this.handleError);
	}

	private handleError (error: any) {
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	console.error(errMsg);
    	return Observable.throw(error);
  	}

}