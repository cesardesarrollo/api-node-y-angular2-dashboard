import { routing } from './app.routes';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule, LocationStrategy, PathLocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS } from 'angular2-jwt';
import { DataTableModule, AutoCompleteModule, CalendarModule } from 'primeng/primeng';
//pipes
import { DateHourPipe }  from './filters/date.pipe';
//services
import { LoginService }  from './login/login.service';
import { AuthService }   from './common/auth.service';
import { AuthGuard } 	  	from './common/auth.guard';
import { profileService } from './_tester/profile/profile.service';
import { AccountService } from './account/account.service';
import { AgreementService } from './admin/agreements/agreements.service';
import { CourseService } from './admin/courses/services/courses.service';
//components
import { AppComponent }  from './app';
import { Login }  from './login/login';
import { Signup }  from './signup/signup';
import { Account }  from './account/account';
import { Home }  from './home/home';
import { Administrator }  from './administrator/administrator';
import { AdminHome }  from './admin/home/home';
import { Test }  from './admin/test/test';
import { Capacitation }  from './admin/capacitation/capacitation';
import { Certification }  from './admin/certification/certification';
import { Task, TaskCreate, TaskEdit }  from './admin/task';
import { Subscription }  from './admin/subscription/subscription';
import { Plan }  from './admin/plan/plan';
import { Levels, LevelsCreate, LevelsEdit }  from './admin/levels';
import { Lessons, LessonsCreate, LessonsEdit }  from './admin/lessons';
import { Platforms, PlatformsCreate, PlatformsEdit }  from './admin/platforms';
import { Courses, CoursesCreate, CoursesEdit }  from './admin/courses/';
import { Categories, CategoriesCreate, CategoriesEdit } from './admin/categories';
import { Producers, ProducersCreate, ProducersEdit } from './admin/producer';
import { Agreements }  from './admin/agreements';
import { Deal, DealEdit }  from './admin/agreements/deal';
import { Terms, TermsEdit }  from './admin/agreements/terms';
import { Tester } from './tester';
import { TesterHome } from './_tester/home';
import { Devices, CreateDevice, EditDevice } from './_tester/devices';
import { TCapacitation, TCourseDetail, MyCourses, TLessonDetail } from './_tester/capacitation';
import { TCertification } from './_tester/certification';
import { Profile } from './_tester/profile';
import { TTasks } from './_tester/tasks';
import { TFinantial } from './_tester/finantial';
import { Matrices, MatricesCreate, MatricesEdit, MatricesPreview }  from './admin/matrices';
import { AdminDevices, AdminDevicesCreate, AdminDevicesEdit  }  from './admin/devices';
import { Exams, ExamsCreate, ExamsQuestion } from './admin/exams';
import { Rins, RinsCreate } from './admin/rins';
import { RinSeverity, RinSeverityCreate } from './admin/rin-severity';
import { RinStatus, RinStatusCreate } from './admin/rin-status';
import { RinError, RinErrorCreate } from './admin/rin-error';
import { Pricing, PricingCreate } from './admin/pricing';

@NgModule({
  imports: [ BrowserModule, routing, ReactiveFormsModule, FormsModule, HttpModule, CommonModule, DataTableModule, AutoCompleteModule, CalendarModule ],       // module dependencies
  declarations: [ AppComponent, DateHourPipe, Login, Signup, Account, Home,
  				  Administrator, AdminHome, Test, Capacitation, Certification,
  				  Task, TaskCreate, TaskEdit, Subscription, Plan, Levels, LevelsCreate, LevelsEdit,
  				  Lessons, LessonsCreate, LessonsEdit, Platforms, PlatformsCreate, 
  				  PlatformsEdit, Courses, CoursesCreate, CoursesEdit, Deal,
  				  DealEdit, Terms, TermsEdit, Tester, TesterHome, Devices, 
  				  CreateDevice,EditDevice, TCapacitation, TCourseDetail, MyCourses,TLessonDetail, TCertification, Profile, TTasks, TFinantial,
  				  Agreements, Categories, CategoriesEdit, CategoriesCreate,
	            Producers, ProducersEdit, ProducersCreate, Matrices, MatricesCreate, MatricesEdit, MatricesPreview, 
	            AdminDevices, AdminDevicesCreate, AdminDevicesEdit, Exams, ExamsCreate, ExamsQuestion, 
	            Rins, RinsCreate, RinSeverity, RinSeverityCreate, RinStatus, RinStatusCreate, RinError, RinErrorCreate,
	            Pricing, PricingCreate,
  				 ],   // components and directives
  bootstrap: [ AppComponent ],     // root component
  providers: [ AuthGuard, LoginService, AuthService, profileService, AccountService,
  			   AgreementService, CourseService, {provide: LocationStrategy, useClass: HashLocationStrategy }
  			 ] // services
})

export class AppModule { }