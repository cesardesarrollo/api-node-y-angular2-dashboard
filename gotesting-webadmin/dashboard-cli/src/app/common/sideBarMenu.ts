import { Component } from '@angular/core';

@Component({
    selector    : 'sidebar-menu',
    template    : ` <ul>
        <li>
          <a [routerLink]="['administrator']">
            <i class="material-icons">home</i> Home
          </a>
        </li>
        <li>
          <a [routerLink]="['/test']">
            <i class="material-icons">assignment</i> Test
          </a>
        </li>
        <li>
          <a class="active" [routerLink]="['/capacitation']">
            <i class="material-icons">lightbulb_outline</i> Capacitation
          </a>
        </li>
        <li>
          <a [routerLink]="['/certification']">
            <i class="material-icons">grade</i> Certification
          </a>
        </li>
        <li>
          <a [routerLink]="['/task']">
            <i class="material-icons">list</i> Tasks
          </a>
        </li>
        <li>
          <a [routerLink]="['/subscription']">
            <i class="material-icons">payment</i> Subscription
          </a>
        </li>
        <li>
          <a [routerLink]="['/plans']">
            <i class="material-icons">today</i> Plans
          </a>
        </li>
        <li>
          <a [routerLink]="['/settings']">
            <i class="material-icons">settings</i> Settings</a>
        </li>
      </ul>`
})

export class SideBarMenu{
    constructor(){}
}