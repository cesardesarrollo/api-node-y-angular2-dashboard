import { Injectable }     from "@angular/core";
//import { User }           from "./user.interface";
import { contentHeaders } from "./headers";
import { Http, Headers }  from "@angular/http";
import { Router }         from "@angular/router";
import { environment }     from '../../environments/environment';

import { AuthHttp, 
         AuthConfig, 
         AUTH_PROVIDERS, 
         JwtHelper }      from 'angular2-jwt';

const baseUrl = environment.base_url_api;

@Injectable()
export class AuthService {

  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private router: Router, public http: Http ) {}

  signinUser(email,password) {
    let body = JSON.stringify({ email, password });
    let msg;
    return this.http.post(
            baseUrl + '/users/login',
            body,
            { headers: contentHeaders }
        )
        .map( (res) => {
          if( res) {
            this.deleteLocalStorage;
            localStorage.setItem('id_token', res.json().response.token);
            msg = { success: res.json().response.success, token : res.json().response.token, msg: res.json().response.message };
          } else {
            msg = { success: res.json().response.success, token : res.json().response.token, msg: res.json().response.message  };
          }
          return msg;
        });           
  }

  deleteLocalStorage() {
    localStorage.removeItem('id_token');
  }

  logout() {
    this.deleteLocalStorage;
    this.router.navigate(['/login']);
  }
  
  useJwtHelper() {
    var token = localStorage.getItem('id_token');
    return this.jwtHelper.decodeToken(token);
  }

  isAuthenticated() {
    var id_token  = localStorage.getItem('id_token');
    if (id_token) {
      return true;
    } else {
      return false;
    }
  }

  getUserToken(){
    let msg;
    if ( this.isAuthenticated() ){
      return localStorage.getItem('id_token');
    } else {
      msg = { success : false, token : null, msg : "Usuario no autenticado"}
      return msg;
    }
  }

}