# DashboardCli

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.16.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/). 
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

Paso 1
Instalaciones iniciales no necesariamente tiene que ser esa version pero ami me funciona adecuadamente
v6.5.0 enlace: https://nodejs.org/download/release/v6.5.0/
Paso 2
Para crear deploy o build para producción 
Instalación de Angular CLI 
npm install -g angular-cli@1.0.0-beta.16

Paso 3 clona el repositorio 
https://Javiercito_Orozco@bitbucket.org/SyeSoftware/gotesting-webadmin.git

3.1 cd nombrerepo/dashboard-cli
3.2 nombrerepo/dashboard-cli$ sudo ng --version //ver version y verificar que este instalado angular-cli
3.3 nombrerepo/dashboard-cli$ sudo ng serve //ejecuta entorno para desarrollo 
3.4 nombrerepo/dashboard-cli$ sudo ng --prod //genera deploy o builds en este 
//caso el archivo environment.prod.ts ya tiene que contener el base URL de la API ejemplo:
 export const environment = {
  production: true,
  base_url_api: 'http://ec2-54-164-166-88.compute-1.amazonaws.com:3000/api'
};

//los deploys generados son guardados en el directorio /dist




