'use strict';
var validator   = require('validator');
var mailer      = require('../middleware/mailer')._transporter(); 
var random      = require('../middleware/random'); 

module.exports = function(User) {

    /*
     *  ACCOUNT ACTIVATION
     *  { @params: email, activationCode }
     */
    User.accountActivation = function(cb){
        
    };
    /*
     *  LOGIN
     *  { @params: email, password }
     */
    // User.authentication = function(data, cb){
    //     var ONE_WEEK = 60 * 60 * 24 * 7;
    //     console.log( data );
    //     User.login({
    //         email    : data.email,
    //         password : data.password,
    //         ttl      : ONE_WEEK
    //     }, function(err, accessToken){
    //         if( err ) cb(err,null);
    //         cb( null, accessToken );
    //     });
    // }
    //  // ADD PATH TO LOGIN
    // User.remoteMethod('authentication', {
    //     accepts : [
    //         { arg : 'email', type : 'string', required : true},
    //         { arg : 'password', type : 'string', required : true}
    //     ],
    //     returns : { arg : 'msg',  type: 'object'},
    //     http    : { path : '/login', verb : 'post'}
    // });

    /*
     *  RESET PASSWORD
     *  { @params: email } 
     */ 
    User.resetPassword = function(email, cb){
        if( !validator.isEmail( email ) ){
            cb('Esto no es un formato de correo valido', null);
        } else {
            var new_password = random._generate(10, true);
            User.update({email : email}, { 'password' : new_password }, function( error, res ){
                if( error ) cb(error, null);
                cb(null, res);
            });
        }
    };

    // ADD PATH TO RESET PASSWORD
    User.remoteMethod('resetPassword', {
        accepts : [
            { arg : 'email', type : 'string', required : true}
        ],
        returns : { arg : 'msg',  type: 'string'},
        http    : { path : '/reset/password', verb : 'post'}
    });

    User.authenticate = function(data, cb){
        console.log(data);
        User.login( data, 'user', function( err, response ){
            if( err ) cb(err, null);
            cb( null, response );
        });
    }
    //ADD LOGIN PATH
    User.remoteMethod('authenticate', {
        accepts : [
            { arg : 'email', type : 'string', required: true},
            { arg : 'password', type : 'string', required: true}
        ],
        returns : { arg: 'response', type : 'object'},
        http    : { path : '/login', verb : 'post' }
    })
};
