var mail    = {},
    mailer  = require('nodemailer');

mail._transporter = function() {
    return mailer.createTransport({
        host 	: 'mail.syesoftware.com',
        secure 	: false,
        port 	: 26,
        auth 	: {
            user : "no-reply@syesoftware.com",
            pass : "Syesoftware2015"
        },
        tls 	: {
            rejectUnauthorized : false
        }
    });
};


mail._sender = function( mailOptions, to, callback ){
    //SEND EMAIL
    this._transporter.sendMail( mailOptions, function( error, info ){
        if( error ){
            callback( error, null );
        } else {
            callback( null, info);
        };
    });
};


module.exports = mail;