var random = {};

random._generate = function( CodeLength, characters ){
    var text;
	if ( characters )
		text = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#$%!";
	else
		text = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	var new_code = "";
	for (var i = 0; i < CodeLength; i++) {
		new_code += text.charAt(Math.floor(Math.random() * text.length));
	};
	return new_code;
};

module.exports = random;